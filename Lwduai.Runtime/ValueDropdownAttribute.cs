// the attribute for ValueDropdownDrawer
// see ValueDropdownDrawer for documentation

using System;
using UnityEngine;

namespace Lwduai
{
[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
public class ValueDropdownAttribute : PropertyAttribute
{
    public string Callback { get; private set; }

    public ValueDropdownAttribute(string callback)
    {
        Callback = callback;
    }
}
} // namespace