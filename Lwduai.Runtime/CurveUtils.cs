// This static utility class contains functions for evaluating response curves

// common preset cheat-sheet
// AngleDown - Angle --> m=-2, k=0, b=1, c=0.5
// AngleUp - Angle --> m=2, k=0, b=0, c=0.5
// BellCurveDown - Normal --> m=1, k=0.7, b=0, c=0
// BellCurveUp - Normal --> m=-1, k=0.7, b=1, c=0
// DecayBackward - DecayVariant --> m=2, k=10, b=-0.09, c=9.2
// DecayForward - DecayVariant --> m=0.6, k=10, b=-0.09, c=-1.2
// HillBackward - Quadratic --> m=-7, k=-4.1, b=1.15, c=-1.55
// HillForward - Quadratic --> m=-0.24, k=5.08, b=1, c=-0.32
// LineLeft - Linear --> m=-1, b=1, c=0
// LineRight - Linear --> m=1, b=1, c=1
// MesaBackward - Logit --> m=0.5, k=-6.3, b=-5.5, c=-0.1
// MesaForward - Logit --> m=2, k=-6.3, b=-5.5, c=0.1
// ParabolaDown - Quadratic --> m=-4, k= 2/4/6/8/etc, b=1, c=0.5
// ParabolaUp - Quadratic --> m=4, k= 2/4/6/8/etc, b=0, c=0.5
// SigmoidBackward - Logistic --> m=-10, k=1, b=0, c=0.5
// SigmoidForward - Logistic --> m=10, k=1, b=0, c=0.5
// SigmoidDown - Logit --> m=16, k=5, b=2.2, c=0
// SigmoidUp - Logit --> m=8, k=-5, b=-2.2, c=0
// SigmoidDownFlat - Quadratic --> m=16, k=5, b=0.5, c=0.5
// SigmoidUpFlat - Quadratic --> m=-16, k=5, b=0.5, c=0.5
// Sine - Sine --> m=1, k=1.5, b=0, c=0
// Step - Step --> m=0.5, k=3, b=0, c=0
// ThresholdBackward - Linear --> m=-1000, k=0, b=0, c=0.5
// ThresholdForward - Linear --> m=1000, k=0, b=0, c=0.5
// ThresholdBackwardAngled - Linear --> m=-3.2, b=0, c=0.7
// ThresholdForwardAngled - Linear --> m=3.2, b=0, c=0.4
// Mound - Square --> m=-1, k=3.8, b=0.5, c=0.3, e=8.4
// Trough - Square --> m=-1, k=3.6, b=0.5, c=-2.9, e=5.5

// hill-left - Logistic --> m=-10.4, k=2, b=0, c=0.5
// hill-right - Logistic --> m-10, k=2, b=0, c=0.5
// fountain-right - Logit --> m=2, k=-6.1, b=-5.5, c=0
// fountain-left - Logit --> m= 0.51, k=-6.3, b=-5.5, c=0.02
// s-sqr-fwd - Logistic --> m = 500, k = 2.1, b=-0.5, c=0.5
// s-sqr-bck - Logistic --> m = -500, k = 2.1, b=-0.5, c=0.5
// s-sqr-angled-fwd - Logistic --> m=10, k=2.1, b=-0.5, c=0.5
// s-sqr-angled-bck - Logistic --> m=-10, k=2.1, b=-0.5, c=0.5

// logistic --> https://www.desmos.com/calculator/npta8rafya
// angle --> https://www.desmos.com/calculator/v9ikn34igj
// linear --> https://www.desmos.com/calculator/abguth4se8
// quadratic --> https://www.desmos.com/calculator/xsstfzswno
// normal --> https://www.desmos.com/calculator/as7cpwot3i
// sine --> https://www.desmos.com/calculator/te4fwfjtwh
// angle --> https://www.desmos.com/calculator/v9ikn34igj
// logit --> https://www.desmos.com/calculator/booioznprb
// step --> https://www.desmos.com/calculator/mkwvtxs589
// square --> https://www.desmos.com/calculator/7pmqxuuaqj
// decay --> https://www.desmos.com/calculator/vnxpawj616
// decayvariant --> https://www.desmos.com/calculator/x5ld4zetut

// the square wave has an extra parameter "s" for how square the corners are, but since there are only four vars to work with (m,k,b,c), s is hardcoded to 100.


using Unity.Mathematics;

namespace Lwduai
{
    public enum CurveType : byte { Linear, Quadratic, Logistic, Logit, Angle, Normal, Sine, Step, Square, Decay, DecayVariant, PassThrough, PassThroughInvert }

    [System.Serializable]
    public struct CurveData
    {
        public CurveType Type; // 1 byte
        public float M; // 4 bytes
        public float K; // 4 bytes
        public float B; // 4 bytes
        public float C; // 4 bytes
        public float E; // 4 bytes

        public CurveData(CurveType type, float m, float k, float b, float c, float e=0)
        {
            Type = type;
            M = m; 
            K = k;
            B = b;
            C = c;
            E = e;
        }

        public void Set(CurveType type, float m, float k, float b, float c, float e=0)
        {
            Type = type;
            M = m; 
            K = k;
            B = b;
            C = c;
            E = e;
        }
    } // 17 bytes total

    public class CurveUtils
    {
        // type --> the type of curve (in)
        // x --> the current value (in)
        // m --> slope
        // k --> bend amount, squish amount
        // b --> verticle shift
        // c --> horizontal shift
        // return --> the new value
        public static float Evaluate(float x, CurveType type, float m, float k, float b, float c, float e)
        {
            switch (type) {
                case CurveType.Linear: return math.clamp((m * (x - c)) + b, 0, 1);
                case CurveType.Quadratic: return Sanitize((m * math.pow(x - c, k)) + b);
                case CurveType.Logistic: return Sanitize((k * (1f / (1f + math.pow(math.pow(math.E, m), -x + c)))) + b);
                case CurveType.Logit: return Sanitize((logn((x + c) / (1f - x - c), m) + b) / k);
                case CurveType.Angle: return math.clamp((m * math.abs(x - c)) + b, 0, 1);
                case CurveType.Normal: return Sanitize(m * math.exp(-30.0f * k * (x - c - 0.5f) * (x - c - 0.5f)) + b);
                case CurveType.Sine: return Sanitize(0.5f * m * math.sin(k * math.PI * ((k * x) - c)) + 0.5f + b);
                case CurveType.Step: return math.clamp((m * math.floor((k * x) - c)) + b, 0, 1);
                case CurveType.Square: return Sanitize(((m / (math.pow(e, e * math.sin((k * x) - c)) + 1f)) - (0.5f * m)) + b);
                case CurveType.Decay: return Sanitize((math.pow(m, m * (x - c)) * k) + b);
                case CurveType.DecayVariant: return Sanitize((math.pow(m, (k * x) - c) * k) + b);
                case CurveType.PassThrough: return x; // user is responsible for ensuring value is clamped
                case CurveType.PassThroughInvert: return -x;
            }
            return 0f;
        }

        public static float Evaluate(float x, in CurveData curve)
        {
            return Evaluate(x, curve.Type, curve.M, curve.K, curve.B, curve.C, curve.E);
        }

        // unlike System.Math.Log(), unity.mathematics doesn't have a version that allows choosing the base
        protected static float logn(float x, float baseVal) { return math.log2(x) / math.log2(baseVal); }

        // ensures that f is not NAN or infinit and also clamps f to between 0 and 1
        protected static float Sanitize(float f)
        {
            if (float.IsNaN(f)) { return 0f; }
            if (float.IsInfinity(f)) { return 0f; }
            return math.clamp(f, 0, 1);
        }
    }
} // namespace