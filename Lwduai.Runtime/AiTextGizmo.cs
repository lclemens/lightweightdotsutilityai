// This singleton class draws a gizmo that displays debugging information next to an agent
// that helps you figure out why an agent has chosen a particular action (editor only).
// If you check the checkbox for enable detail, then the scores for each agent will display next to the agent.
//-----------------------------------------------------------------------------------------------------------------------//

using UnityEditor;
using UnityEngine;
using Unity.Entities;
using Unity.Collections;
using Unity.Transforms;
using Unity.Mathematics;

namespace Lwduai
{
    public enum AiTextMode { Detail, QuickInfo, Off }

    public class AiTextGizmo : MonoBehaviour // : Singleton<AiTextGizmo>
    {
        [Tooltip("Enable/Disable AI debug output (default true)")] public AiTextMode m_textMode;
        [Tooltip("An integer number for font size that scales with screen size, 1..n (default 2)")] public int m_fontSize = 2;
        [Tooltip("Color of the text to use (default null)")] public Color m_textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
        [Tooltip("X Offset (default 0)")] public int m_xOffset = 0;
        [Tooltip("Y Offset (default 0)")] public int m_yOffset = 0;

        // This index should correspond to a value in the AiInputType enum.
        [Tooltip("Index of the health input (AiInputType, used with quick-info, -1 to ignore)")] public short m_customInputIndex = 0;

        // It's best to keep this string short
        [Tooltip("Custom prefix string (used with quick-info)")] public string m_customInputPrefix = "Hlth";

        public AiTextMode GetTextMode() { return m_textMode; }

        BlobAssetReference<AiTableDef> m_table;

        EntityManager m_em;
        EntityQuery m_query;
        bool m_isInitialized = false;
        GUIStyle m_style= new GUIStyle();

        // only draw gizmos in the editor
    #if UNITY_EDITOR
        void OnDrawGizmos()
        {
            if (!Application.isPlaying) { return; }
            if (m_textMode == AiTextMode.Off) { return; }
            if (!m_table.IsCreated) {
                m_table = AiUtils.TryGetDwAiTableReference();
                if (!m_table.IsCreated) { return; }
            }
            if (!((World.DefaultGameObjectInjectionWorld != null) && (World.DefaultGameObjectInjectionWorld.IsCreated))) { return; }
            if (!m_isInitialized) {
                m_em = World.DefaultGameObjectInjectionWorld.EntityManager;
                m_query = m_em.CreateEntityQuery(ComponentType.ReadOnly<AiActionData>(), ComponentType.ReadOnly<LocalTransform>());
                m_isInitialized = true;
            }
            
            var xForms = m_query.ToComponentDataArray<LocalTransform>(Allocator.Temp);
            var entities = m_query.ToEntityArray(Allocator.Temp);
            m_query.CompleteDependency(); 

            GUIStyle style = new();
            style.alignment = TextAnchor.UpperLeft;
            style.fontSize = (Screen.height * m_fontSize) / 200;
            style.normal.textColor = m_textColor;

            // for every agent that was found
            for (int e = 0; e < entities.Length; e++) {
                if (m_textMode == AiTextMode.Detail) {
                    Handles.Label(xForms[e].Position + math.up() * 0.125f, AiUtils.MakeAiDetailString(m_em, m_table, entities[e]), style);
                } else if (m_textMode == AiTextMode.QuickInfo) {
                    Handles.Label(xForms[e].Position + math.up() * 0.125f, AiUtils.MakeAiQuickInfoString(m_em, m_table, entities[e], m_customInputIndex, m_customInputPrefix), style);
                }
            }

            xForms.Dispose();
            entities.Dispose();
        }

    #endif
    }
} // namespace