// This contains system groups used by LightweightDotsUtilityAi.

using Unity.Entities;

namespace Lwduai
{
    [UpdateInGroup(typeof(SimulationSystemGroup), OrderFirst = true)]
    [UpdateAfter(typeof(BeginSimulationEntityCommandBufferSystem))]
    public partial class EarlySimulationSystemGroup : ComponentSystemGroup { }
}