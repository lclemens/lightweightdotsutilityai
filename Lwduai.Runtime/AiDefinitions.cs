// This contains definitions for data structures within the blob asset.

using Unity.Collections;
using Unity.Entities;

namespace Lwduai
{
    public enum RangeMaskType : byte {
        None   = 0,        // 0000  0
        Min    = 1,        // 0001  1 - min should be interpreted as AiInputType instead of a float
        Max    = (1 << 1), // 0010  2 - max should be interpreted as AiInputType instead of a float
        Input  = (1 << 2)  // 0100  4 - input dependency
    }

    public struct AiInputDef
    {
        public short InputId;
        public FixedString128Bytes InputName;
        public BlobArray<short> ContributingConsiderationIds;
    }

    public struct AiConsiderationDef
    {
        public short ConsiderationId; // the id of this consideration
        public FixedString128Bytes ConsiderationName;
        public CurveData Curve; // holds information about this consideraton's mathematical curve
        public short InputId; // the id of the input that is used to evaluate this consideration
        public byte RangeMask; // determine if min/max are interpreted as AiInputType or floats.
        public float Min; // holds either AiInputType or a fixed float value (depends on RangeMask)
        public float Max; // holds either AiInputType or a fixed float value (depends on RangeMask)
        public BlobArray<short> ContributingDecisionIds;

        public bool IsInputValid(RangeMaskType type, in DynamicBuffer<AiInputData> inputs)
        {
            switch (type) {
                case RangeMaskType.Input: return ValIsValid(inputs);
                case RangeMaskType.Max: return MaxIsValid(inputs);
                case RangeMaskType.Min: return MinIsValid(inputs);
            }
            return false;
        }

        // returns true if the input value is fresh
        public bool ValIsValid(in DynamicBuffer<AiInputData> inputs) { return inputs[InputId].IsFreshInput; }
    
        // returns true if the the max value is valid (dynamic and fresh, or static)
        public bool MaxIsValid(in DynamicBuffer<AiInputData> inputs) { return !MaxIsDynamic() || inputs[(short)Max].IsFreshInput; }

        // returns true if the the min value is valid (dynamic and fresh, or static)
        public bool MinIsValid(in DynamicBuffer<AiInputData> inputs) { return !MinIsDynamic() || inputs[(short)Min].IsFreshInput; }

        // returns true if all three inputs are valid (min, max, and val)
        // (either static or dynamic and fresh)
        public bool IsEvaluatable(in DynamicBuffer<AiInputData> inputs) { return ValIsValid(inputs) && MaxIsValid(inputs) && MinIsValid(inputs); }

        public int GetMinDynamicInputIndex()
        {
            if (!MinIsDynamic()) { return -1; }
            return (int)Min;
        }

        // returns true if the max value is dynamic
        public bool MaxIsDynamic() { return BitmaskUtils.HasFlag(RangeMask, (byte)RangeMaskType.Max); }

        // returns true if the min value is dynamic
        public bool MinIsDynamic() { return BitmaskUtils.HasFlag(RangeMask, (byte)RangeMaskType.Min); }

        // returns the max value (looks it up in the input buffer if it's dynamic)
        public float GetMaxValue(in DynamicBuffer<AiInputData> inputs) { return BitmaskUtils.HasFlag(RangeMask, (byte)RangeMaskType.Max) ? inputs[(short)Max].Value : Max; }

        // returns the min value (looks it up in the input buffer if it's dynamic)
        public float GetMinValue(in DynamicBuffer<AiInputData> inputs) { return BitmaskUtils.HasFlag(RangeMask, (byte)RangeMaskType.Min) ? inputs[(short)Min].Value : Min; }
    }

    public struct AiDecisionDef
    {
        public short DecisionId; // the id of this decision
        //public BlobString DecisionName;
        public FixedString128Bytes DecisionName;
        public float Weight; // weight can fudge this decision to make it more or less likely to be chosen.
        public bool IsEnabled; // you can enable/disable a decision with this
        public BlobArray<short> ConsiderationIds;
    }

    public struct AiProfileDef
    {
        public short ProfileId; // the id of this profile
        public FixedString128Bytes ProfileName;
        public BlobArray<short> DecisionIds; // a list of decisision ids
    }

    public struct AiTableDef
    {
        public BlobArray<AiInputDef> Inputs; // one slot for each input
        public BlobArray<AiConsiderationDef> Considerations; // one slot for each consideration
        public BlobArray<AiDecisionDef> Decisions; // one slot for each decision
        public BlobArray<AiProfileDef> Profiles; // one slot for each profile

        // returns the input ID of the specified string, or -1 if not found.
        public short FindInputId(FixedString128Bytes strId)
        {
            for (short i = 0; i < Inputs.Length; i++) {
                if (strId == Inputs[i].InputName) { return i; }
            }
            return -1;
        }

        // returns the consideration ID of the specified string, or -1 if not found.
        public short FindConsiderationId(FixedString128Bytes strId)
        {
            for (short c = 0; c < Considerations.Length; c++) {
                if (strId == Considerations[c].ConsiderationName) { return c; }
            }
            return -1;
        }

        // returns the decision ID of the specified string, or -1 if not found.
        public short FindDecisionId(FixedString128Bytes strId)
        {
            for (short d = 0; d < Decisions.Length; d++) {
                if (strId == Decisions[d].DecisionName) { return d; }
            }
            return -1;
        }

        // returns the profile ID of the specified string, or -1 if not found.
        public short FindProfileId(FixedString128Bytes strId)
        {
            for (short p = 0; p < Profiles.Length; p++) {
                if (strId == Profiles[p].ProfileName) { return p; }
            }
            return -1;
        }
    }
}