// Place this authoring tool on an agent prefab that will go through baking
// to make it controlled by Utility AI systems.

using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using System.Linq;

namespace Lwduai
{
    public class AiAgentAuthoring : MonoBehaviour
    {
        // TODO: this would be more flexible if it did't use AiProfileType
        [Tooltip("The default profiles to use for this agent. They can be swapped in and out at runtime.")]
        [ValueDropdown("FetchNames")]
        public List<string> Profiles;

        public List<string> FetchNames()
        {
            if (AiDatabase == null) { return new List<string>(); }
            return AiDatabase.MakeProfileNames();
        }

        public AiDbRecord AiDatabase;
    }

    public class AiAgentBaker : Baker<AiAgentAuthoring>
    {
        public override void Bake(AiAgentAuthoring authoring)
        {
            Entity entity = GetEntity(authoring, TransformUsageFlags.None);

            List<string> uniqueProfiles = authoring.Profiles.Distinct().ToList();

            // This holds the current profiles for the agent
            DynamicBuffer<AiProfileData> profiles = AddBuffer<AiProfileData>(entity);
            for (int p = 0; p < uniqueProfiles.Count; p++) {
                //profiles.Add(new AiProfileData { ProfileId = authoring.Profiles[p] });
                short profileId = authoring.AiDatabase.FindProfileIndex(uniqueProfiles[p]);
                if (profileId != -1) {
                    profiles.Add(new AiProfileData { ProfileId = profileId });
                }
            }

            // This holds a buffer of input data for the agent.
            // It is often a simple copy of another variable (such as a health value).
            // These numbers do not necessarily need to be normalized, but they can be depending on developer preference.
            // One or more systems should be responsible for filling them.
            // Each slot corresponds to an AiInputType enum value.
            DynamicBuffer<AiInputData> inputs = AddBuffer<AiInputData>(entity);
            for (short i = 0; i < authoring.AiDatabase.Inputs.Count; i++) { inputs.Add(new AiInputData { Value = 0f, RefCount = 0, IsFreshInput = false }); }

            // This holds a buffer of consideration scores
            // It may be used by some systems to "stack" scores.
            // It also is used by the debugging gizmo.
            // It is also used to cache evaluations so that if multiple actions want to use the same consideration, only one needs to evaluate the curve equation.
            // Each slot corresponds to an AiConsiderationType enum value.
            DynamicBuffer<AiConsiderationScoreData> considerations = AddBuffer<AiConsiderationScoreData>(entity);
            for (short c = 0; c < authoring.AiDatabase.Considerations.Count; c++) { considerations.Add(new AiConsiderationScoreData { IsEvaluated = false, Score = 0f }); }

            // This holds a buffer of decision scores for the agent.
            // It may be used by some systems to "stack" scores.
            // It also is used by the debugging gizmo.
            // Each slot corresponds to an AiDecisionType enum value.
            // During a calculation frame, some of the slots may be updated, and then the rest finished at the end of the frame.
            // This creates a temporary inconsistency in the decision buffer.
            // Most reaction systems will use the AiActionData, which won't update until the end of the frame, so it won't affect them.
            // If there are any stacking calculations, they will simply use cached data during that frame, so they should keep working fine.
            DynamicBuffer<AiDecisionScoreData> decisions = AddBuffer<AiDecisionScoreData>(entity);
            for (short d = 0; d < authoring.AiDatabase.Decisions.Count; d++) { decisions.Add(new AiDecisionScoreData { Score = 0f, IsUsedByProfile = false, TemporaryScore = 0f, IsComplete = false }); }

            // This is the final action that various reaction systems can use to know the current action
            AddComponent(entity, new AiActionData { Action = -1, OldAction = -1 });

            // This keep tracks of the current winning decision.
            AddComponent(entity, new AiWinnerData { Index = -1, Score = float.MinValue });
        }
    }
}