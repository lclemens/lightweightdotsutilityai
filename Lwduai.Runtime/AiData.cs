// This holds various components and tags used by the AI ECS systems


using System.Linq;
using Unity.Collections;
using Unity.Entities;
using UnityEngine.Profiling;

namespace Lwduai
{
    public struct DisableAiTag : IComponentData { }

    public struct AiTableData : IComponentData
    {
        public BlobAssetReference<AiTableDef> Reference;

        // returns the input ID of the specified string, or -1 if not found.
        public short FindInputId(FixedString128Bytes strId)
        {
            for (short i = 0; i < Reference.Value.Inputs.Length; i++) {
                if (strId == Reference.Value.Inputs[i].InputName) { return i; }
            }
            return -1;
        }

        // returns the consideration ID of the specified string, or -1 if not found.
        public short FindConsiderationId(FixedString128Bytes strId)
        {
            for (short c = 0; c < Reference.Value.Considerations.Length; c++) {
                if (strId == Reference.Value.Considerations[c].ConsiderationName) { return c; }
            }
            return -1;
        }

        // returns the decision ID of the specified string, or -1 if not found.
        public short FindDecisionId(FixedString128Bytes strId)
        {
            for (short d = 0; d < Reference.Value.Decisions.Length; d++) {
                if (strId == Reference.Value.Decisions[d].DecisionName) { return d; }
            }
            return -1;
        }

        // returns the profile ID of the specified string, or -1 if not found.
        public short FindProfileId(FixedString128Bytes strId)
        {
            for (short p = 0; p < Reference.Value.Profiles.Length; p++) {
                if (strId == Reference.Value.Profiles[p].ProfileName) { return p; }
            }
            return -1;
        }
    }

    public struct AiAgentTableData : IComponentData
    {
        public BlobAssetReference<AiTableDef> Reference;
    }

    // These items go in a buffer that resides on the AI Agent
    // The developer is responsible for filling these values via one or more systems.
    // It's useful if you fill most of them with a single system because it results in less write-access systems.
    [InternalBufferCapacity(0)]
    public struct AiInputData : IBufferElementData
    {
        public float Value; // 4
        public ushort RefCount; // 2
        public bool IsFreshInput; // 1

        public bool NeedsCalculation()
        {
            if (IsFreshInput) { return false; }
            if (RefCount <= 0) { return false; }
            return true;
        }

        public AiInputData(float val)
        {
            Value = val;
            RefCount = 0;
            IsFreshInput = true;
        }
    } // total bytes : 4 + 2 + 1 --> 7 (8 due to packing).


    // An agent entity should have a buffer of these, which holds the evaluations for ALL considerations.
    // There is one slot per consideration type.
    [InternalBufferCapacity(0)]
    public struct AiConsiderationScoreData : IBufferElementData
    {
        // This score will always hold either a cached value or an up-to-date value.
        public float Score; // 4

        // True when evaluated (gets reset before each new scoring).
        public bool IsEvaluated; // 1
    } // total bytes: 4 + 1 --> 5 (8 due to packing)

    // This is placed on agents as a DynamicBuffer to allow them to have multiple profiles
    // These can be added and removed at runtime to change their behavior.
    public struct AiProfileData : IBufferElementData
    {
        public short ProfileId;
    }

    // This buffer element holds decisions score data for an agent.
    [InternalBufferCapacity(0)]
    public struct AiDecisionScoreData : IBufferElementData
    {
        public float TemporaryScore; // [4] holds a temporary score that is used whenever IsComplete is false.
        public float Score; // [4] holds the "official" score whenever IsComplete is true.

        // I could put these bools in a 1 byte bitmask to save space... but would it even matter?
        // I think burst will treat these bools as 4 bytes anyways without doing special packing stuff.
        public bool IsComplete; // [1] true if the score is complete - either via an easy-out or because all considerations were scored.
        public bool IsUsedByProfile; // [1] true if at least one of the profiles uses this decision.
    } // total bytes: 4 + 4 = 1 + 1 --> 10 (12 due to packing)

    // This component holds the current action for an agent.
    public struct AiActionData : IComponentData
    {
        public short Action; // every action corresponds with a decision
        public short OldAction; // holds the previous action
        public float Score; // the action's score (provided for convenience)
        public bool IsChanged() { return (Action != OldAction); }
    }

    // This component is used to hold the current winning decision for every agent.
    public struct AiWinnerData : IComponentData
    {
        public short Index; // holds current winning index (corresponds to AiDecisionType)
        public float Score; // holds the high score
    }

    public struct AiFirstFrameTag : IComponentData { }
    public struct AiFrame1Tag : IComponentData { }
    public struct AiFrame2Tag : IComponentData { }
    public struct AiFrame3Tag : IComponentData { }
    public struct AiFrame4Tag : IComponentData { }
    public struct AiFrame5Tag : IComponentData { }
    public struct AiFrame6Tag : IComponentData { }
    public struct AiFrame7Tag : IComponentData { }
    public struct AiFrame8Tag : IComponentData { }
    public struct AiFrame9Tag : IComponentData { }
    public struct AiLastFrameTag : IComponentData { }
}