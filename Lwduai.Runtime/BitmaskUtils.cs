// functions for working with bit masks
namespace Lwduai
{
    public static class BitmaskUtils
    {
        //--------------------- ulong (64) --------------------//

        // adds the specified flag to value and returns the new mask
        public static ulong SetFlag(ulong value, ulong flag) { return value | flag; }

        // returns true if value has the specified flag
        public static bool HasFlag(ulong value, ulong flag) { return (value & flag) == flag; }

        // removes the specified flag from value and returns the new mask
        public static ulong UnsetFlag(ulong value, ulong flag) { return value & ~flag; }

        // sets or unsets a flag based on enable.
        public static ulong ToggleFlag(ulong value, ulong flag, bool enable) { return enable ? SetFlag(value, flag) : UnsetFlag(value, flag); }

        // returns the mask with the specified bit set to one.
        public static ulong SetBit(ulong mask, byte index) { return (1u << index) | mask; }

        // returns the mask with the specified bit set to zero.
        public static ulong UnsetBit(ulong mask, byte index) { return mask & ~(1u << index); }

        // returns the mask with the specified flag set to 1 or zero based on enable
        public static ulong ToggleBit(ulong mask, byte index, bool enable) { return enable ? SetBit(mask, index) : UnsetBit(mask, index); }

        // returns true if the specified bit in the given mask is one.
        public static bool HasBit(ulong mask, byte index) { return (mask & (1UL << index)) != 0; }

        // given an index, returns a corresponding mask with just that one bit set
        public static ulong IndexToMaskValueUlong(byte index) { return (ulong)(1u << index); }

        // given a mask with just one bit set (1, 2, 4, 8, 16, etc), returns the corresponding index
        public static byte MaskToIndex(ulong mask) { return (byte)(Unity.Mathematics.math.log2(mask)); }

        // convert the bitmask to a string
        public static string ToString(ulong value) => System.Convert.ToString((long)value, 2).PadLeft(64, '0');

        // rotate bits by the specified amount to the left and return the new value
        public static ulong RotateLeft(ulong value, byte amount) => unchecked((value << amount) | (value >> (64 - amount)));

        // rotate bits by the specified amount to the right and return the new value
        public static ulong RotateRight(ulong value, byte amount) => unchecked((value >> amount) | (value << (64 - amount)));


        //--------------------- uint (32) --------------------//

        // adds the specified flag to value and returns the new mask
        public static uint SetFlag(uint value, uint flag) { return value | flag; }

        // returns true if value has the specified flag
        public static bool HasFlag(uint value, uint flag) { return (value & flag) == flag; }

        // removes the specified flag from value and returns the new mask
        public static uint UnsetFlag(uint value, uint flag) { return value & ~flag; }

        // sets or unsets a flag based on enable.
        public static uint ToggleFlag(uint value, uint flag, bool enable) { return enable ? SetFlag(value, flag) : UnsetFlag(value, flag); }

        // returns the mask with the specified bit set to one.
        public static uint SetBit(uint mask, byte index) { return (1u << index) | mask; }

        // returns the mask with the specified bit set to zero.
        public static uint UnsetBit(uint mask, byte index) { return mask & ~(1u << index); }

        // returns the mask with the specified bit set to one or zero based on enable.
        public static uint ToggleBit(uint mask, byte index, bool enable) { return enable ? SetBit(mask, index) : UnsetBit(mask, index); }

        // returns true if the specified bit in the given mask is one.
        public static bool HasBit(uint mask, byte index) { return (mask & (1u << index)) != 0; }

        // given an index, returns a corresponding mask with just that one bit set
        public static uint IndexToMaskValueUint(byte index) { return (uint)(1u << index); }

        // given a mask with just one bit set (1, 2, 4, 8, 16, etc), returns the corresponding index
        public static byte MaskToIndex(uint mask) { return (byte)(Unity.Mathematics.math.log2(mask)); }

        // convert the bitmask to a string
        public static string ToString(uint value) => System.Convert.ToString(value, 2).PadLeft(32, '0');

        // returns the left and right halves as one merged value
        public static ulong Merge(uint left, uint right) => (left | right << 32);

        // returns the left half of the bits
        public static uint GetLeft(ulong value) => (uint)(value);

        // returns the right half of the bits
        public static uint GetRight(ulong value) => (uint)(value >> 32);

        // rotate bits by the specified amount to the left and return the new value
        public static uint RotateLeft(uint value, byte amount) => unchecked((value << amount) | (value >> (32 - amount)));

        // rotate bits by the specified amount to the right and return the new value
        public static uint RotateRight(uint value, byte amount) => unchecked((value >> amount) | (value << (32 - amount)));

        //------------------------ ushort (16) --------------------//

        // adds the specified flag to value and returns the new mask
        public static ushort SetFlag(ushort value, ushort flag) { return (ushort)(value | flag); }

        // returns true if value has the specified flag
        public static bool HasFlag(ushort value, ushort flag) { return (value & flag) == flag; }

        // removes the specified flag from value and returns the new mask
        public static ushort UnsetFlag(ushort value, ushort flag) { return (ushort)(value & ~flag); }

        // sets or unsets a flag based on enable.
        public static ushort ToggleFlag(ushort value, ushort flag, bool enable) { return enable ? SetFlag(value, flag) : UnsetFlag(value, flag); }

        // returns the mask with the specified bit set to one.
        public static ushort SetBit(ushort mask, byte index) { return (ushort)((1u << index) | mask); }

        // returns the mask with the specified bit set to zero.
        public static ushort UnsetBit(ushort mask, byte index) { return (ushort)(mask & ~(1u << index)); }

        // returns the mask with the specified flag set to 1 or zero based on enable
        public static ushort ToggleBit(ushort mask, byte index, bool enable) { return enable ? SetBit(mask, index) : UnsetBit(mask, index); }

        // returns true if the specified bit in the given mask is one.
        public static bool HasBit(ushort mask, byte index) { return (mask & (1UL << index)) != 0; }

        // given an index, returns a corresponding mask with just that one bit set
        public static ushort IndexToMaskValueUshort(byte index) { return (ushort)(1u << index); }

        // given a mask with just one bit set (1, 2, 4, 8, 16, etc), returns the corresponding index
        public static byte MaskToIndex(ushort mask) { return (byte)(Unity.Mathematics.math.log2(mask)); }

        // convert the bitmask to a string
        public static string ToString(ushort value) => System.Convert.ToString(value, 2).PadLeft(16, '0');

        // returns the left and right halves as one merged value
        public static uint Merge(ushort left, ushort right) => (uint)(left | right << 16);

        // returns the left half of the bits
        public static ushort GetLeft(uint value) => (ushort)(value);

        // returns the right half of the bits
        public static ushort GetRight(uint value) => (ushort)(value >> 16);

        // rotate bits by the specified amount to the left and return the new value
        public static ushort RotateLeft(ushort value, byte amount) => unchecked((ushort)((value << amount) | (value >> (16 - amount))));

        // rotate bits by the specified amount to the right and return the new value
        public static ushort RotateRight(ushort value, byte amount) => unchecked((ushort)((value >> amount) | (value << (16 - amount))));

        //------------------------ byte (8) --------------------//

        // adds the specified flag to value and returns the new mask
        public static byte SetFlag(byte value, byte flag) { return (byte)(value | flag); }

        // returns true if value has the specified flag
        public static bool HasFlag(byte value, byte flag) { return (value & flag) == flag; }

        // removes the specified flag from value and returns the new mask
        public static byte UnsetFlag(byte value, byte flag) { return (byte)(value & ~flag); }

        // sets or unsets a flag based on enable.
        public static byte ToggleFlag(byte value, byte flag, bool enable) { return enable ? SetFlag(value, flag) : UnsetFlag(value, flag); }

        // returns the mask with the specified bit set to one.
        public static byte SetBit(byte mask, byte index) { return (byte)((1u << index) | mask); }

        // returns the mask with the specified bit set to zero.
        public static byte UnsetBit(byte mask, byte index) { return (byte)(mask & ~(1u << index)); }

        // returns the mask with the specified bit set to one or zero based on enable.
        public static byte ToggleBit(byte mask, byte index, bool enable) { return enable ? SetBit(mask, index) : UnsetBit(mask, index); }

        // returns true if the specified bit in the given mask is one.
        public static bool HasBit(byte mask, byte index) { return (mask & ((byte)1u << index)) != 0; }

        // given an index, returns a corresponding mask with just that one bit set
        public static byte IndexToMaskValueByte(byte index) { return (byte)(1u << index); }

        // given a mask with just one bit set (1, 2, 4, 8, 16, etc), returns the corresponding index
        public static byte MaskToIndex(byte mask) { return (byte)Unity.Mathematics.math.log2(mask); }

        // convert the bitmask to a string
        public static string ToString(byte value) => System.Convert.ToString(value, 2).PadLeft(8, '0');

        // returns the left and right halves as one merged value
        public static ushort Merge(byte left, byte right) => (ushort)(left | right << 8);

        // returns the left half of the bits
        public static byte GetLeft(ushort value) => (byte)(value);

        // returns the right half of the bits
        public static byte GetRight(ushort value) => (byte)(value >> 8);

        // rotate bits by the specified amount to the left and return the new value
        public static byte RotateLeft(byte value, byte amount) => unchecked((byte)((value << amount) | (value >> (8 - amount))));

        // rotate bits by the specified amount to the right and return the new value
        public static byte RotateRight(byte value, byte amount) => unchecked((byte)((value >> amount) | (value << (8 - amount))));
    }
} // namespace