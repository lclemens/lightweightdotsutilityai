// This file defines a scriptable object that holds all AI data.
// It also defines all the structures necessary for
// the scriptable object and some helper functions.

using UnityEngine;
using System.Collections.Generic;

namespace Lwduai
{
    [CreateAssetMenu(fileName = "New AI Database", menuName = "Game/AI Database")]
    public class AiDbRecord : ScriptableObject
    {
        public List<AiProfileEntry> Profiles = new List<AiProfileEntry>();
        public List<AiDecisionEntry> Decisions = new List<AiDecisionEntry>();
        public List<AiConsiderationEntry> Considerations = new List<AiConsiderationEntry>();
        public List<AiInputEntry> Inputs = new List<AiInputEntry>();

        public bool IsEmpty()
        {
            return (Profiles.Count <= 0) && (Decisions.Count <= 0) && (Considerations.Count <= 0) && (Inputs.Count <= 0);
        }

        public List<string> MakeDecisionNames()
        {
            List<string> vals = new List<string>();
            for (short d = 0; d < Decisions.Count; d++) {
                vals.Add(Decisions[d].Name);
            }
            return vals;
        }

        public void RenameDecision(string oldName, string newName)
        {
            for (short p = 0; p < Profiles.Count; p++) {
                for (short d = 0; d < Profiles[p].DecisionNames.Count; d++) {
                    if (Profiles[p].DecisionNames[d] == oldName) {
                        Profiles[p].DecisionNames[d] = newName;
                    }
                }
            }
        }

        public void RenameConsideration(string oldName, string newName)
        {
            for (short d = 0; d < Decisions.Count; d++) {
                for (short c = 0; c < Decisions[d].ConsiderationNames.Count; c++) {
                    if (Decisions[d].ConsiderationNames[c] == oldName) {
                        Decisions[d].ConsiderationNames[c] = newName;
                    }
                }
            }
        }

        public void RenameInput(string oldName, string newName)
        {
            for (short c = 0; c < Considerations.Count; c++) {
                Considerations[c].FixInputName(oldName, newName);
            }
        }

        public short FindDecisionIndex(string name)
        {
            for (short d = 0; d < Decisions.Count; d++) {
                if (Decisions[d].Name == name) { return d; }
            }
            return -1;
        }

        public List<string> MakeProfileNames()
        {
            List<string> vals = new List<string>();
            for (short p = 0; p < Profiles.Count; p++) {
                vals.Add(Profiles[p].Name);
            }
            return vals;
        }

        public short FindProfileIndex(string name)
        {
            for (short p = 0; p < Profiles.Count; p++) {
                if (Profiles[p].Name == name) { return p; }
            }
            return -1;
        }

        public List<string> MakeConsiderationNames()
        {
            List<string> vals = new List<string>();
            for (short c = 0; c < Considerations.Count; c++) {
                vals.Add(Considerations[c].Name);
            }
            return vals;
        }

        public short FindConsiderationIndex(string name)
        {
            for (short d = 0; d < Considerations.Count; d++) {
                if (Considerations[d].Name == name) { return d; }
            }
            return -1;
        }

        public List<string> MakeInputNames()
        {
            List<string> vals = new List<string>();
            for (short c = 0; c < Inputs.Count; c++) {
                vals.Add(Inputs[c].Name);
            }
            return vals;
        }

        public string GetInputNameAtIndex(short idx)
        {
            if (idx < 0) { return "None"; }
            return Inputs[idx].Name;
        }

        public short FindInputIndex(string name)
        {
            for (short d = 0; d < Inputs.Count; d++) {
                if (Inputs[d].Name == name) { return d; }
            }
            return -1;
        }

        public void WriteEnumsToFile(string strFolder, string strFileName = "AiEnums.cs", bool enableReimport = true)
        {
            List<string> declarations = new List<string>();
            List<string> enumVals = new List<string>();
            
            // inputs
            foreach (AiInputEntry input in Inputs) { enumVals.Add(input.Name); }
            declarations.Add(EnumUtils.MakeEnumDeclaration("AiInputType", enumVals, "short", true));

            // considerations
            enumVals.Clear();
            foreach (AiConsiderationEntry consideration in Considerations) { enumVals.Add(consideration.Name); }
            declarations.Add(EnumUtils.MakeEnumDeclaration("AiConsiderationType", enumVals, "short", true));

            // decisions
            enumVals.Clear();
            foreach (AiDecisionEntry decision in Decisions) { enumVals.Add(decision.Name); } 
            declarations.Add(EnumUtils.MakeEnumDeclaration("AiDecisionType", enumVals, "short", true));

            // profiles
            enumVals.Clear();
            foreach (AiProfileEntry profile in Profiles) { enumVals.Add(profile.Name); }
            declarations.Add(EnumUtils.MakeEnumDeclaration("AiProfileType", enumVals, "short", true));

            // write to file
            EnumUtils.WriteDeclarationsToFile(strFileName, declarations, enableReimport, strFolder, "This file was auto-generated by Tools/Lightweight Ai/Settings.");
        }
    } // class AiDbRecord

    // PROFILE ENTRY
    [System.Serializable]
    public class AiProfileEntry
    {
        [Tooltip("The name of the profile.")]
        public string Name;

        [Tooltip("A list of decisions that this profile will evaluate when choosing an action.")]
        public List<string> DecisionNames = new List<string>();

        [HideInInspector] // this i only used by the visualization tool
        public bool IsFoldoutEnabled = false;

        public AiProfileEntry(string name)
        {
            Name = name;
        }
    } // AiProfileEntry

    // DECISION ENTRY
    [System.Serializable]
    public class AiDecisionEntry
    {
        [Tooltip("Name of the decision.")]
        public string Name;

        [Tooltip("Weight is an extra fudge factor to give advantage to a decision. (default 1).")]
        public float Weight = 1f;

        [Tooltip("Unchecking this can disable the decision (useful for debugging) (default true).")]
        public bool IsEnabled = true;

        [Tooltip("A list of all the considerations that factor into the decision.")]
        public List<string> ConsiderationNames = new List<string>();

        [HideInInspector]
        public bool IsFoldoutEnabled = false;

        public AiDecisionEntry() { }

        public AiDecisionEntry(string name)
        {
            Name = name;
        }

        public AiDecisionEntry(string name, float weight, bool enabled)
        {
            Name = name;
            Weight = weight;
            IsEnabled = enabled;
        }
    } // AiDecisionEntry

    // CONSIDERATION ENTRY
    [System.Serializable]
    public class AiRangeEntry
    {
        [Tooltip("Checking this will allow the min value to be obtained from an input. (default false)")]
        public bool MinIsVariable = false;

        [Tooltip("Checking this will allow the max value to be obtained from an input. (default false)")]
        public bool MaxIsVariable = false;

        [Tooltip("The static value used if the minIsVariable is unchecked. (default 0)")]
        public float Min = 0f;

        [Tooltip("The static value used if the maxIsVariable is unchecked. (default 1)")]
        public float Max = 1f;

        [Tooltip("The input value to use if minIsVariable is checked. (default first input)")]
        public string MinInputName;

        [Tooltip("The input value to use if maxIsVariable is checked. (default first input)")]
        public string MaxInputName;
    } // AiRangeEntry


    [System.Serializable]
    public class AiConsiderationEntry
    {
        [Tooltip("Name of the consideration.")]
        public string Name;

        [Tooltip("Specify the curve type. (default Linear)")]
        public CurveData Curve = new CurveData { M = 0, K = 0, B = 0, C = 0, Type = CurveType.Linear };

        [Tooltip("The input to use for the value in this consideration.")]
        public string InputName;

        [Tooltip("Default range is 0 to 1. Enable this to set custom ranges. (default disabled)")]
        public bool EnableRanges = false;

        public AiRangeEntry Ranges = new AiRangeEntry();

        [BoundedCurve(0, 0, 1, 1, 2)]
        [SerializeField]
        public AnimationCurve CurveDisplay = new AnimationCurve();

        [HideInInspector]
        public bool IsFoldoutEnabled = false;

        public AiConsiderationEntry() { }
        public AiConsiderationEntry(string name, CurveData curve, string inputName, bool enableRanges, AiRangeEntry ranges, AnimationCurve curveDisplay, bool isFoldoutEnabled)
        {
            Name = name;
            Curve = curve;
            InputName = inputName;
            EnableRanges = enableRanges;
            Ranges = ranges;
            CurveDisplay = curveDisplay;
            IsFoldoutEnabled = isFoldoutEnabled;
        }
        public AiConsiderationEntry(string name)
        {
            Name = name;
        }

        //// Nonlinear version
        //public void RefreshCurveDisplay()
        //{
        //    CurveDisplay.keys = new Keyframe[0];
        //    for (int i = 0; i <= 20; i++) {
        //        float sample = i * 0.05f;
        //        CurveDisplay.AddKey(sample, CurveUtils.Evaluate(sample, Curve));
        //    }
        //}

        public bool DependsOnInput(string inputName)
        {
            if (inputName == InputName) { return true; }
            if (!EnableRanges) { return false; }
            if (Ranges.MinIsVariable && Ranges.MinInputName == inputName) { return true; }
            if (Ranges.MaxIsVariable && Ranges.MaxInputName == inputName) { return true; }
            return false;
        }

        public void FixInputName(string oldName, string newName)
        {
            if (InputName == oldName) { InputName = newName; }
            if (Ranges.MinInputName == oldName) { Ranges.MinInputName = newName; }
            if (Ranges.MaxInputName == oldName) { Ranges.MaxInputName = newName; }
        }

        // Linear version
        public void RefreshCurveDisplay()
        {
#if UNITY_EDITOR
            CurveDisplay.keys = new Keyframe[0];
            for (int s = 0; s <= 20; s++) {
                float sample = s * 0.05f;
                CurveDisplay.AddKey(sample, CurveUtils.Evaluate(sample, Curve));
            }
            for (int i = 0; i < CurveDisplay.length; i++) {
                UnityEditor.AnimationUtility.SetKeyLeftTangentMode(CurveDisplay, i, UnityEditor.AnimationUtility.TangentMode.Linear);
                UnityEditor.AnimationUtility.SetKeyRightTangentMode(CurveDisplay, i, UnityEditor.AnimationUtility.TangentMode.Linear);
            }
#endif // UNITY_EDITOR
        }
    } // AiConsiderationEntry

    // INPUT ENTRY
    [System.Serializable]
    public class AiInputEntry
    {
        [Tooltip("The name of the input.")]
        public string Name;

        public AiInputEntry(string name)
        {
            Name = name;
        }
    }

} // namespace