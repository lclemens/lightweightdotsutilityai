// Typically there is only one of these in the scene.
// You can configure it in the editor.
// It creates a singleton that contains profiles, which are made of decisions,
// which are made of considerations, which are made of inputs.
//
// Press the GenerateEnums button to generate enums that can be used to reference various inputs/considerations/decisions/profiles.
//
// 0.25 * 30 -> 7.5 frames
// 0.25 * 60 --> 15 frames

using UnityEngine;
using Unity.Entities;

namespace Lwduai
{
    public struct AiFrameStateData : IComponentData
    {
        // Frame -1 is the normal state (scoring not running)
        // Frames 0..N-1 are the expensive input frames
        // Frame N is the final reaction system frame.
        public sbyte FrameNumber;
    }

    public struct AiScoreConfigData : IComponentData
    {
        public sbyte LastFrame; // this is "N" - the last frame (the frame where the action is fresh and presented to the user)
        public float ScoringInterval; // the interval at which scoring happens
        public bool EnableAutoEndFrameScore; // true to enable an end-frame scoring system
    }

    public class AiSingletonAuthoring : MonoBehaviour
    {
        [Tooltip("These are extra frames that you can use for filling expensive inputs that are action depedent. See docs. (default 0).")]
        public sbyte ExtraFrameCount = 0;

        [Tooltip("Set this to your game's target FPS - it is only used to warn if you if your config is invalid. (default 30)")]
        public ushort TargetFPS = 30;

        [Tooltip("The interval at which scoring happens, measured in seconds. (default 0.25)")]
        public float ScoringInterval = 0.25f;

        [Tooltip("Auto-runs a scoring at the end of each frame. See docs. (default true)")]
        public bool EnableAutoEndFrameScore = true;

        [Tooltip("A scriptable object that contains all the AI data")]
        public AiDbRecord AiDatabase;
    }

    public class AiSingletonBaker : Baker<AiSingletonAuthoring>
    {
        public override void Bake(AiSingletonAuthoring authoring)
        {
            if (authoring.AiDatabase == null) { return; }
            if (authoring.AiDatabase.Inputs == null) { return; }

            DependsOn(authoring.AiDatabase);
            Entity entity = GetEntity(authoring, TransformUsageFlags.None);
            AddComponent(entity, new AiTableData { Reference = AiUtils.CreateBlobAsset(authoring.AiDatabase) });
            AddComponent(entity, new AiFrameStateData { FrameNumber = -1 });
            AddComponent(entity, new AiScoreConfigData { LastFrame = (sbyte)(authoring.ExtraFrameCount + 1), ScoringInterval = authoring.ScoringInterval, EnableAutoEndFrameScore = authoring.EnableAutoEndFrameScore });
        }
    }
}