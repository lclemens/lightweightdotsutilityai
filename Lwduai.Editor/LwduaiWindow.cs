// This file contains a class for the LWDUAI EditorWindow
// It is the first window and responsible for holding all the other tabs.
// It is the first tab, and it stores various settings.

#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace Lwduai
{
    public class LwduaiWindow : EditorWindow
    {
        public static GUIStyle MakeFixedWidthStyle(float width, GUIStyle style)
        {
            GUIStyle newStyle = new GUIStyle(style);
            newStyle.fixedWidth = 200;
            return newStyle;
        }

        static ProfileWindow m_profileWindow = null;
        static DecisionWindow m_decisionWindow = null;
        static ConsiderationWindow m_considerationWindow = null;
        static InputWindow m_inputWindow = null;

        string m_enumOutputFolder = "Assets/Scripts/Generated/";

        string m_soPath;

        [SerializeField] AiDbRecord m_dbSo;

        public void InitDatabase(string path)
        {
            if (path.Length > 0) {
                m_dbSo = AssetDatabase.LoadAssetAtPath<AiDbRecord>(path);
                if (m_dbSo == null) { UnityEngine.Debug.Log($"Failed to load database at: {path}"); return; }
                m_soPath = path;
                return;
            }
            UnityEngine.Debug.Log("No database path found.");
        }

        static LwduaiWindow() { }

        // This function is responsible for putting the window in the menu and showing it.
        [MenuItem("Tools/Lightweight AI")]
        static void Initialize()
        {
            // Get existing open window or if none, make a new one.
            LwduaiWindow mainWindow = GetWindow<LwduaiWindow>(false, "Settings");
            mainWindow.Show();

            // note - i tried calling GetWindow() for the other windows here, but
            // for some reason the pointers get reset to null sometimes,
            // so i had to do it in OnGui() instead.
        }

        void OnEnable()
        {
            // note, this gets called IMMEDATLY after calling the GetWindow() inside Initialize()
            // so m_profileWindow and the other member vars may be null.
            // i tried calling GetWindow() for m_profileWindow and other windows here,
            // but for some reason it tried to open them in new windows instead of tabs,
            // so i had to do it in OnGUI instead.

            // Attempt to load last used database from path.
            InitDatabase(EditorPrefs.GetString("LwduiaSoPath"));
            m_enumOutputFolder = EditorPrefs.GetString("EnumOutputFolder", m_enumOutputFolder);
        }

        void OnDestroy() { }

        bool EnsureTabs()
        {
            if (m_profileWindow != null) { return true; }

            m_profileWindow = GetWindow<ProfileWindow>("Profiles", false, typeof(LwduaiWindow));
            m_decisionWindow = GetWindow<DecisionWindow>("Decisions", false, typeof(LwduaiWindow));
            m_considerationWindow = GetWindow<ConsiderationWindow>("Considerations", false, typeof(LwduaiWindow));
            m_inputWindow = GetWindow<InputWindow>("Inputs", false, typeof(LwduaiWindow));

            if (m_profileWindow != null) { SetDatabaseToWindows(); }
            return m_profileWindow != null;
        }

        public AiDbRecord GetDatabase() { return m_dbSo; }

        void OnGUI()
        {
            // setup the other tabs
            EnsureTabs();

            // a field to specify the database scriptable object
            EditorGUI.BeginChangeCheck();
            m_dbSo = EditorGUILayout.ObjectField(new GUIContent("AI Database SO", "The AI database scriptable object - drag-and-drop it here."), m_dbSo, typeof(ScriptableObject), true) as AiDbRecord;
            if (EditorGUI.EndChangeCheck()) {
                m_soPath = AssetDatabase.GetAssetPath(m_dbSo);
                EditorPrefs.SetString("LwduiaSoPath", m_soPath);
                SetDatabaseToWindows();
            }

            // a text field to specify the enum output folder
            EditorGUI.BeginChangeCheck();
            m_enumOutputFolder = EditorGUILayout.TextField(m_enumOutputFolder);
            if (EditorGUI.EndChangeCheck()) { EditorPrefs.SetString("EnumOutputFolder", m_enumOutputFolder); }

            EditorGUILayout.BeginHorizontal();
            // a button to regenerate the enums
            if (GUILayout.Button("Generate Enums")) {
                UnityEngine.Debug.Log($"Regenerating enums to {m_enumOutputFolder} ");
                m_dbSo.WriteEnumsToFile(m_enumOutputFolder);
            }

            // a button for forcing domain reload
            if (GUILayout.Button("Force Domain Reload")) { UnityEditor.AssetDatabase.Refresh(); }
            EditorGUILayout.EndHorizontal();
        }

        public void SetDatabaseToWindows()
        {
            m_profileWindow.SetDatabase(m_dbSo);
            m_decisionWindow.SetDatabase(m_dbSo);
            m_considerationWindow.SetDatabase(m_dbSo);
            m_inputWindow.SetDatabase(m_dbSo);
        }
    }
}

#endif // UNITY_EDITOR