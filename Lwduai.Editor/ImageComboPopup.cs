// Displays an image combo popup
// It is made to be used by EditorWindows.
// You have to use the callback to set your item because I couldn't find a way to do with a ref.
// Unfortunately, because of the way popups work, we can't set the output value after PopupWindow.Show()
// because it will never get called if the user clicks somewhere else in the window (which closes the popup).

using UnityEditor;
using System.Collections.Generic;
using UnityEngine;

namespace Lwduai
{
    public class ImageComboPopup : PopupWindowContent
    {
        // DELEGATE DECLARATIONS
        public delegate void SelectItemCallbackDelegate(int index);
        public delegate void ToggleCallbackDelegate(bool open);
        public delegate void CloseCallbackDelegate();

        // INPUTS
        public List<Texture2D> Images;
        public SelectItemCallbackDelegate SelectItemCallback = null; // tells you when an items is selected
        public ToggleCallbackDelegate ToggleCallback = null; // tells you when the window opens or closes
        public CloseCallbackDelegate CloseCallback = null; // tells you when the window closes.

        // PRIVATE
        Vector2 m_size;
        Vector2 m_scrollPos;
        float m_itemHeight = 20f;

        // constructor without delegate
        public ImageComboPopup(List<Texture2D> images, float width = 300, float height = 150, float itemHeight = 20)
        {
            m_size.x = width;
            m_size.y = height;
            Images = images;
            m_itemHeight = itemHeight;
        }

        // constructor with delegate
        public ImageComboPopup(List<Texture2D> images, SelectItemCallbackDelegate callback = null, float width = 300, float height = 150, float itemHeight = 20)
        {
            SelectItemCallback = callback;
            m_size.x = width;
            m_size.y = height;
            Images = images;
            m_itemHeight = itemHeight;
        }

        // refreshed by unity whenever the window should be redrawn
        public override void OnGUI(Rect rect)
        {
            m_scrollPos = GUILayout.BeginScrollView(m_scrollPos, GUILayout.ExpandWidth(false));
            for (int i = 0; i < Images.Count; i++) {
                //if (GUILayout.Button(new GUIContent("", Images[i]), GUILayout.Width(rect.width - 8), GUILayout.Height(20))) {
                if (GUILayout.Button(Images[i], GUILayout.Width(rect.width - 20), GUILayout.Height(m_itemHeight))) {
                    SelectItemCallback?.Invoke(i);
                    editorWindow.Close();
                }
            }
            GUILayout.EndScrollView();
        }

        // called when the popup is closed
        public override void OnClose()
        {
            ToggleCallback?.Invoke(false);
            CloseCallback?.Invoke();
        }
        // called when the popup is opened
        public override void OnOpen() { ToggleCallback?.Invoke(true); }

        // returns the current window size
        public override Vector2 GetWindowSize() { return m_size; }

    } // FilterComboPopup
} // namespace