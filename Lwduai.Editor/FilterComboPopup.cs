// Displays a filter
// It is made to be used by EditorWindows.
// You have to use the callback to set your item because I couldn't find a way to do with a ref.
// Unfortunately, because of the way popups work, we can't set the output value after PopupWindow.Show()
// because it will never get called if the user clicks somewhere else in the window (which closes the popup).
//
// Example of use:
//    List<string> myList = {"uno", "dos", "tres"};
//    if (EditorGUI.DropdownButton(r, new GUIContent("MyButton"), FocusType.Passive)) {
//        PopupWindow.Show(r, new FilterComboPopup(myList, ItemSelectedCallback));
//    }
//    void ItemSelectedCallback(int index, string value)
//    {
//        // set your data here using value.
//        UnityEngine.Debug.Log($"You selected index {(int)index} with value {value}.");
//    }

using UnityEditor;
using System.Collections.Generic;
using UnityEngine;

namespace Lwduai
{
    public class FilterComboPopup : PopupWindowContent
    {
        // DELEGATE DECLARATIONS
        public delegate void SelectItemCallbackDelegate(int index, string value);
        public delegate void ToggleCallbackDelegate(bool open);
        public delegate void CloseCallbackDelegate();

        // INPUTS
        public string SearchFilter = ""; // only values that match this string will display
        public List<string> PossibleValues = new List<string>(); // a list of all possible values
        public SelectItemCallbackDelegate SelectItemCallback = null; // tells you when an items is selected
        public ToggleCallbackDelegate ToggleCallback = null; // tells you when the window opens or closes
        public CloseCallbackDelegate CloseCallback = null; // tells you when the window closes.

        // PRIVATE
        Vector2 m_size;
        Vector2 m_scrollPos;

        // constructor without delegate
        public FilterComboPopup(List<string> possibleValues, string defaultFilter = "", float width = 300, float height = 150)
        {
            m_size.x = width;
            m_size.y = height;
            SearchFilter = defaultFilter;
            PossibleValues = possibleValues;
        }

        // constructor with delegate
        public FilterComboPopup(List<string> possibleValues, SelectItemCallbackDelegate callback = null, string defaultFilter = "", float width = 300, float height = 150)
        {
            SelectItemCallback = callback;
            m_size.x = width;
            m_size.y = height;
            SearchFilter = defaultFilter;
            PossibleValues = possibleValues;
        }

        // refreshed by unity whenever the window should be redrawn
        public override void OnGUI(Rect rect)
        {
            SearchFilter = GUILayout.TextField(SearchFilter, GUI.skin.FindStyle("ToolbarSearchTextField"));
            m_scrollPos = GUILayout.BeginScrollView(m_scrollPos, GUILayout.ExpandWidth(false));
            for (int i = 0; i < PossibleValues.Count; i++) {
                if (PossibleValues[i].Contains(SearchFilter, System.StringComparison.OrdinalIgnoreCase)) {
                    if (GUILayout.Button(PossibleValues[i])) {
                        SelectItemCallback?.Invoke(i, PossibleValues[i]);
                        editorWindow.Close();
                    }
                }
            }
            GUILayout.EndScrollView();
        }

        // called when the popup is closed
        public override void OnClose()
        {
            ToggleCallback?.Invoke(false);
            CloseCallback?.Invoke();
        }
        // returns the current window size
        public override Vector2 GetWindowSize() { return m_size; }
        // called when the popup is opened
        public override void OnOpen() { ToggleCallback?.Invoke(true); }

    } // FilterComboPopup
} // namespace