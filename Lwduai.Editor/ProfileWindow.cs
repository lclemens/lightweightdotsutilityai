// This file contains a class for the Profile EditorWindow

#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;
using UnityEditorInternal;
using System.Collections.Generic;
using System.Linq;

namespace Lwduai
{
    public class ProfileWindow : EditorWindow
    {
        bool m_refreshMe = false;
        string m_searchString = "";
        AiDbRecord m_dbSo = null;
        [SerializeField] ReorderableList m_profileList;
        List<string> m_allDecisionNames;
        Vector2 m_scrollPos;
        SerializedObject m_dbProp;
        Dictionary<string, ReorderableList> m_decisionListDict = new Dictionary<string, ReorderableList>();

        public void SetDatabase(AiDbRecord db) { m_dbSo = db; }

        void OnGUI()
        {
            // The scriptable object database must be set to display anything
            if (m_dbSo == null) { 
                EditorGUILayout.LabelField("Database is null.");
                m_dbSo = GetWindow<LwduaiWindow>()?.GetDatabase();
                return;
            }

            // update the scriptable object (load it from serialization)
            if (m_dbProp != null) { m_dbProp.Update(); }

            // Top search string field
            if (m_refreshMe) {
                this.Repaint();
                m_refreshMe = false;
            }
            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginChangeCheck();
            m_searchString = GUILayout.TextField(m_searchString, GUI.skin.FindStyle("ToolbarSearchTextField"));
            if (GUILayout.Button("", GUI.skin.FindStyle("ToolbarSearchCancelButton"))) {
                m_searchString = "";
                GUI.FocusControl(null); // Remove focus if cleared
            }
            if (EditorGUI.EndChangeCheck()) { m_refreshMe = true; }
            EditorGUILayout.EndHorizontal();

            // Fetch a list of all possible decision names
            m_allDecisionNames = m_dbSo.MakeDecisionNames();

            // List of profiles with scroll view
            m_scrollPos = GUILayout.BeginScrollView(m_scrollPos, false, false);
            // make sure the reorderable list exists and that it's tied to the decisions list in the database
            if (m_profileList == null) {
                m_profileList = new ReorderableList(m_dbSo.Profiles, typeof(AiProfileEntry), true, false, true, true);
                m_profileList.drawElementCallback = DrawProfileItemCallback;
                m_profileList.elementHeightCallback = GetProfileItemHeightCallback;
                m_profileList.onAddCallback = AddProfileItemCallback;
                m_dbProp = new SerializedObject(m_dbSo);
                m_profileList.serializedProperty = m_dbProp.FindProperty("Profiles");
            }
            EditorGUI.BeginChangeCheck();
            m_profileList.DoLayoutList();
            if (EditorGUI.EndChangeCheck()) { EditorUtility.SetDirty(m_dbSo); }
            GUILayout.EndScrollView();

            m_dbProp.ApplyModifiedProperties();
        }

        // Callback invoked whenever a new decision is added to the list.
        void AddProfileItemCallback(ReorderableList list)
        {
            if (m_dbSo.Profiles.Count <= 0) {
                m_dbSo.Profiles.Add(new AiProfileEntry("NewProfile"));
                return;
            }
            // new name - previous name plus one.
            string previousName = m_dbSo.Profiles[m_dbSo.Profiles.Count - 1].Name;
            m_dbSo.Profiles.Add(new AiProfileEntry(StringUtils.MakeUniqueWithNumberSuffix(previousName)));
        }

        // Callback invoked whenever the height for a particular decision item is needed.
        // This accounts for the decision names sublist which can increase or decrease the required height.
        float GetProfileItemHeightCallback(int profileIndex)
        {
            // Hide this item if it doesn't match the search string
            if (StringUtils.DoesNotMatchSearchString(m_searchString, m_dbSo.Profiles[profileIndex].Name)) { return 0f; }

            // Only display one line if the foldout is close
            if (!m_dbSo.Profiles[profileIndex].IsFoldoutEnabled) { return EditorGUIUtility.singleLineHeight; }

            // Calculate the height... first line plus all items.
            float height = EditorGUIUtility.singleLineHeight * 5; // initial line plus padding.
            height += Mathf.Max(0, m_dbSo.Profiles[profileIndex].DecisionNames.Count - 1) * EditorGUIUtility.singleLineHeight * 1.3f;
            return height;
        }

        // Called whenever a profile item needs to be drawn
        // There are two modes - with the foldout enabled and disabled.
        void DrawProfileItemCallback(Rect rect, int profileIndex, bool isActive, bool isFocused)
        {
            if (m_dbSo == null) { return; }
            if (m_dbSo.Profiles.Count <= 0) { return; }

            AiProfileEntry currentProfile = m_dbSo.Profiles[profileIndex]; // alias

            if (StringUtils.DoesNotMatchSearchString(m_searchString, currentProfile.Name)) { return; }


            Rect r = new Rect(rect.x, rect.y, 10, EditorGUIUtility.singleLineHeight);

            // LINE 1 (always drawn)
            string foldoutName = currentProfile.IsFoldoutEnabled ? "" : currentProfile.Name;
            currentProfile.IsFoldoutEnabled = EditorGUI.Foldout(r, currentProfile.IsFoldoutEnabled, foldoutName);
            r.x += 20; // skip past the foldout triangle

            // Stop drawing if the foldout is closed
            if (!currentProfile.IsFoldoutEnabled) { return; }

            // Name
            EditorGUIUtility.labelWidth = 50;
            r.width = 250;
            EditorGUI.BeginChangeCheck();
            string newName = EditorGUI.TextField(r, new GUIContent("Name:"), currentProfile.Name, LwduaiWindow.MakeFixedWidthStyle(200, EditorStyles.textField));
            newName = EnumUtils.MakeStringEnumCompatible(newName);
            if (EditorGUI.EndChangeCheck() && m_dbSo.FindProfileIndex(newName) < 0) {
                currentProfile.Name = newName;
            }

            // LINE 2
            r.y += EditorGUIUtility.singleLineHeight + 5; // skip to next line
            r.x = rect.x;

            // Decision names list
            r.width = 400;
            var profilesProp = m_dbProp.FindProperty("Profiles");
            var profileProp = profilesProp.GetArrayElementAtIndex(profileIndex);
            SerializedProperty decisionNamesProp = profileProp.FindPropertyRelative("DecisionNames");
            if (decisionNamesProp == null) { UnityEngine.Debug.Log("decisionNamesProp is null"); return; }
            string key = profileProp.propertyPath;
            if (!m_decisionListDict.TryGetValue(key, out ReorderableList decisionList)) {
                decisionList = new ReorderableList(decisionNamesProp.serializedObject, decisionNamesProp, true, false, true, true);
                m_decisionListDict.Add(key, decisionList);
                decisionList.drawElementCallback = (rect, index, isActive, isFocused) => { DrawDecisionNameItemCallback(rect, index, isActive, isFocused, profileIndex); };
                decisionList.onCanAddCallback = (list) => CanAddDecisionCallback(profileIndex);
                decisionList.onAddCallback = (list) => AddDecisionCallback(profileIndex);
            }
            decisionList.DoList(r);
        }

        // Called whenever a decision name is added to any decision names list.
        // This is intercepted so that we can ensure that the newly added
        // decision name won't be a duplicate of a previous one.
        void AddDecisionCallback(int profileIndex)
        {
            // attempt to add a decision name that doesn't already exist
            for (int i = 0; i < m_allDecisionNames.Count; i++) {
                if (!m_dbSo.Profiles[profileIndex].DecisionNames.Contains(m_allDecisionNames[i])) {
                    m_dbSo.Profiles[profileIndex].DecisionNames.Add(m_allDecisionNames[i]);
                    return;
                }
            }
        }

        // callback to determine if the add button for a decision list is enabled.
        bool CanAddDecisionCallback(int profileIndex)
        {
            // if the current decision names has all the same items as the list of all available decision names,
            // then return false, which will disable the addition button.
            return m_allDecisionNames.Except(m_dbSo.Profiles[profileIndex].DecisionNames).Count() > 0;
        }

        // called back whenever a decision name needs to be drawn.
        void DrawDecisionNameItemCallback(Rect rect, int decisionNameIndex, bool isActive, bool isFocused, int profileIndex)
        {
            AiProfileEntry currentProfile = m_dbSo.Profiles[profileIndex]; // alias

            // fetch the decision name for this decision index
            string selectedOption = currentProfile.DecisionNames[decisionNameIndex];

            // make a list of all decisions that are not already in the list
            List<string> filteredList = m_allDecisionNames.Except(currentProfile.DecisionNames).ToList();
            filteredList.Add(selectedOption);

            // setup position rect
            EditorGUIUtility.labelWidth = 0;
            Rect r = new Rect(rect.x, rect.y + 5, rect.width, EditorGUIUtility.singleLineHeight);

            // show a button with the current selected name
            if (EditorGUI.DropdownButton(r, new GUIContent(selectedOption), FocusType.Passive)) {
                PopupWindow.Show(r, new FilterComboPopup(filteredList, ItemSelectedCallback, "", r.width));
            }
            void ItemSelectedCallback(int index, string value)
            {
                currentProfile.DecisionNames[decisionNameIndex] = value;
                Repaint();
            }
        }
    }
} // namespace

#endif // UNITY_EDITOR