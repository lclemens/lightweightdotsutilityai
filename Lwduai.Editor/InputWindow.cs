// This file contains a class for the Input EditorWindow

#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;
using UnityEditorInternal;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine.Windows;

namespace Lwduai
{
    public class InputWindow : EditorWindow
    {
        bool m_refreshMe = false;
        string m_searchString = "";
        AiDbRecord m_dbSo = null;
        public void SetDatabase(AiDbRecord db) { m_dbSo = db; }
        Vector2 m_scrollPos;
        [SerializeField] ReorderableList m_inputList = null;

        private void OnEnable() { }

        void OnGUI()
        {
            // The scriptable object database must be set to display anything
            if (m_dbSo == null) { 
                EditorGUILayout.LabelField("Database is null.");
                m_dbSo = GetWindow<LwduaiWindow>()?.GetDatabase();
                return;
            }

            // Top search string field
            if (m_refreshMe) {
                this.Repaint();
                m_refreshMe = false;
            }
            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginChangeCheck();
            m_searchString = GUILayout.TextField(m_searchString, GUI.skin.FindStyle("ToolbarSearchTextField"));
            if (EditorGUI.EndChangeCheck()) { m_refreshMe = true; }
            if (GUILayout.Button("", GUI.skin.FindStyle("ToolbarSearchCancelButton"))) {
                m_searchString = "";
                GUI.FocusControl(null); // Remove focus if cleared
            }
            if (EditorGUI.EndChangeCheck()) { m_refreshMe = true; }
            EditorGUILayout.EndHorizontal();

            // List of inputs with scroll view
            m_scrollPos = GUILayout.BeginScrollView(m_scrollPos, false, false);

            if (m_inputList == null) {
                m_inputList = new ReorderableList(m_dbSo.Inputs, typeof(AiInputEntry), true, true, true, true);
                m_inputList.elementHeight = EditorGUIUtility.singleLineHeight;
                m_inputList.drawElementCallback = DrawInputItemCallback;
                m_inputList.onRemoveCallback = RemoveInputItemCallback;
                m_inputList.elementHeightCallback = GetInputItemHeightCallback;
                m_inputList.onAddCallback = AddInputItemCallback;
            }

            EditorGUI.BeginChangeCheck();
            m_inputList.DoLayoutList();
            if (EditorGUI.EndChangeCheck()) { EditorUtility.SetDirty(m_dbSo); }

            GUILayout.EndScrollView();
        }

        void AddInputItemCallback(ReorderableList list)
        {
            if (m_dbSo.Inputs.Count <= 0) { 
                m_dbSo.Inputs.Add(new AiInputEntry("NewInput"));
                return;
            }
            // new name - previous name plus one.
            string previousName = m_dbSo.Inputs[m_dbSo.Inputs.Count - 1].Name;
            m_dbSo.Inputs.Add(new AiInputEntry(StringUtils.MakeUniqueWithNumberSuffix(previousName)));
        }


        // callback that is invoked whenever the height of an input item needs to be calculated.
        float GetInputItemHeightCallback(int inputIndex)
        {
            if (StringUtils.DoesNotMatchSearchString(m_searchString, m_dbSo.Inputs[inputIndex].Name)) { return 0f; }
            return EditorGUIUtility.singleLineHeight; // input items are always a single line
        }

        // called whenever the input reorderable list needs to draw an input
        void DrawInputItemCallback(Rect rect, int inputIndex, bool isActive, bool isFocused)
        {
            if (m_dbSo == null) { return; }
            if (StringUtils.DoesNotMatchSearchString(m_searchString, m_dbSo.Inputs[inputIndex].Name)) { return; }

            EditorGUI.BeginChangeCheck();
            string oldName = m_dbSo.Inputs[inputIndex].Name;
            string newName = EditorGUI.TextField(rect, m_dbSo.Inputs[inputIndex].Name);
            if (EditorGUI.EndChangeCheck()) {
                newName = EnumUtils.MakeStringEnumCompatible(newName);
                if ((m_dbSo.FindInputIndex(newName) < 0)) {
                    m_dbSo.Inputs[inputIndex].Name = newName;
                    m_dbSo.RenameInput(oldName, newName);
                }
            }
        }

        // This callback is invoked whenever the designer removes an input from the list
        // Checks are done to alert the designer if deletion of a particular item will
        // cause problems downstream.
        void RemoveInputItemCallback(ReorderableList list)
        {
            string inputName = m_dbSo.Inputs[list.index].Name;
            string msg = "";
            for (int c = 0; c < m_dbSo.Considerations.Count; c++) {
                if (m_dbSo.Considerations[c].DependsOnInput(inputName)) {
                    if (msg.Length > 0) { msg += ", "; }
                    msg += $"{m_dbSo.Considerations[c].Name}";
                }
            }

            bool doDelete = false;
            if (msg.Length > 0) {
                string deleteMessage = $"Warning, deleting {inputName} will affect these considerations: {msg}";
                if (EditorUtility.DisplayDialog("Are you sure?", deleteMessage, "OK", "Cancel")) { doDelete = true; }
            } else {
                doDelete = true;
            }

            if (doDelete) {
                m_dbSo.Inputs.RemoveAt(list.index);
                EditorUtility.SetDirty(m_dbSo);
            }
        }
    }
} // namespace

#endif // UNITY_EDITOR