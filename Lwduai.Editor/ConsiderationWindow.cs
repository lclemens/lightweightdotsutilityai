// This file contains a class for the Consideration EditorWindow

#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;
using UnityEditorInternal;
using System.Collections.Generic;

namespace Lwduai
{
    public class CurvePreset
    {
        public string Name;
        public CurveData Curve;
        public CurvePreset(string name, CurveType type, float m, float k, float b, float c, float e=0f)
        {
            Name = name;
            Curve.Type = type;
            Curve.M = m;
            Curve.K = k;
            Curve.B = b;
            Curve.C = c;
            Curve.E = e;
        }
    }

    public class ConsiderationWindow : EditorWindow
    {
        public static CurvePreset[] PRESETS = {
            new CurvePreset("AngleDown", CurveType.Angle, -2, 0, 1, 0.5f),
            new CurvePreset("AngleUp", CurveType.Angle, 2, 0, 0, 0.5f),
            new CurvePreset("BellCurveDown", CurveType.Normal, 1, 0.7f, 0, 0),
            new CurvePreset("BellCurveUp", CurveType.Normal, -1, 0.7f, 1, 0),
            new CurvePreset("DecayBackward", CurveType.DecayVariant, 2, 10, -0.09f, 9.2f),
            new CurvePreset("DecayForward", CurveType.DecayVariant, 0.6f, 10, -0.09f, -1.2f),
            new CurvePreset("HillBackward", CurveType.Quadratic, -7, -4.1f, 1.15f, -1.55f),
            new CurvePreset("HillForward", CurveType.Quadratic, -0.24f, 5.08f, 1, -0.32f),
            new CurvePreset("LineLeft", CurveType.Linear, -1, 0, 1, 0),
            new CurvePreset("LineRight", CurveType.Linear, 1, 0, 1, 1),
            new CurvePreset("MesaBackward", CurveType.Logit, 0.5f, -6.3f, -5.5f, -0.1f),
            new CurvePreset("MesaForward", CurveType.Logit, 2, -6.3f, -5.5f, 0.1f),
            new CurvePreset("ParabolaDown", CurveType.Quadratic, -4, 2, 1, 0.5f),
            new CurvePreset("ParabolaUp", CurveType.Quadratic, 4, 2, 0, 0.5f),
            new CurvePreset("SigmoidBackward", CurveType.Logistic, -10, 1, 0, 0.5f),
            new CurvePreset("SigmoidForward", CurveType.Logistic, 10, 1, 0, 0.5f),
            new CurvePreset("SigmoidDown", CurveType.Logit, 6, 4.4f, 2.2f, -0.002f),
            new CurvePreset("SigmoidUp", CurveType.Logit, 6, -4.4f, -2.2f, 0.002f),
            new CurvePreset("SigmoidDownFlat", CurveType.Quadratic, 16, 5, 0.5f, 0.5f),
            new CurvePreset("SigmoidUpFlat", CurveType.Quadratic, -16, 5, 0.5f, 0.5f),
            new CurvePreset("Sine", CurveType.Sine, 1, 1.5f, 0, 0),
            new CurvePreset("Step", CurveType.Step, 0.5f, 3, 0, 0),
            new CurvePreset("ThresholdBackward", CurveType.Linear, -1000, 0, 0, 0.5f),
            new CurvePreset("ThresholdForward", CurveType.Linear, 1000, 0, 0, 0.5f),
            new CurvePreset("ThresholdBackwardAngled", CurveType.Linear, -2.4f, 0, 1.6f, 0),
            new CurvePreset("ThresholdForwardAngled", CurveType.Linear, 2.5f, 0, -0.8f, 0),
            new CurvePreset("Mound", CurveType.Square, -1, 3.8f, 0.5f, 0.3f, 8.4f),
            new CurvePreset("Trough", CurveType.Square, -1, 3.6f, 0.5f, -2.9f, 5.5f)
        };

        bool m_refreshMe = false;
        string m_searchString = "";
        AiDbRecord m_dbSo = null;
        ReorderableList m_considerationList;
        List<string> m_inputNames;
        List<Texture2D> m_curveImages = new List<Texture2D>();
        Vector2 m_scrollPos;

        enum InputFieldType { Min, Max, Input }

        public void SetDatabase(AiDbRecord db) { m_dbSo = db; }

        private void OnEnable()
        {
            //string path = "Packages/Lightweight DOTS Utility AI/Lwduai.Editor/Assets/"; // no good
            //string path = "Packages/com.lwduai/Editor/Assets/"; // no good
            //string path = "Packages/com.lwduai/Lwduai.Editor/Assets/"; // not tested
            string path = "Packages/com.lwduai/Lwduai.Editor/Assets/"; // this one works
            #if LWDUAI_USE_LOCAL_IMAGES
                path = "Assets/Lwduai/Lwduai.Editor/Assets/";
            #endif

            m_curveImages.Clear();
            foreach (var val in PRESETS) {
                m_curveImages.Add(AssetDatabase.LoadAssetAtPath<Texture2D>($"{path}{val.Name}.png"));
            }
        }

        void OnGUI()
        {
            // The scriptable object database must be set to display anything
            if (m_dbSo == null) {
                EditorGUILayout.LabelField("Database is null.");
                m_dbSo = GetWindow<LwduaiWindow>()?.GetDatabase();
                return;
            }

            // Top search string field
            if (m_refreshMe) {
                this.Repaint();
                m_refreshMe = false;
            }
            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginChangeCheck();
            m_searchString = GUILayout.TextField(m_searchString, GUI.skin.FindStyle("ToolbarSearchTextField"));
            if (EditorGUI.EndChangeCheck()) { m_refreshMe = true; }
            if (GUILayout.Button("", GUI.skin.FindStyle("ToolbarSearchCancelButton"))) {
                m_searchString = "";
                GUI.FocusControl(null); // Remove focus if cleared
            }
            if (EditorGUI.EndChangeCheck()) { m_refreshMe = true; }
            EditorGUILayout.EndHorizontal();

            // rebuild the input names
            m_inputNames = m_dbSo.MakeInputNames();

            // List of considerations with scroll view
            m_scrollPos = GUILayout.BeginScrollView(m_scrollPos, false, false);
            if (m_considerationList == null) {
                m_considerationList = new ReorderableList(m_dbSo.Considerations, typeof(AiConsiderationEntry), true, false, true, true);
                m_considerationList.elementHeight = EditorGUIUtility.singleLineHeight * 5f;
                m_considerationList.drawElementCallback = DrawConsiderationItemCallback;
                m_considerationList.onRemoveCallback = RemoveConsiderationItemCallback;
                m_considerationList.elementHeightCallback = GetConsiderationItemHeightCallback;
                m_considerationList.onAddCallback = AddConsiderationItemCallback;
            }
            // display the consideration list
            EditorGUI.BeginChangeCheck();
            m_considerationList.DoLayoutList();
            if (EditorGUI.EndChangeCheck()) { EditorUtility.SetDirty(m_dbSo); }

            GUILayout.EndScrollView();
        }

        void AddConsiderationItemCallback(ReorderableList list)
        {
            if (m_dbSo.Considerations.Count <= 0) {
                m_dbSo.Considerations.Add(new AiConsiderationEntry("NewConsideration"));
                return;
            }
            // new name - previous name plus one.
            string previousName = m_dbSo.Considerations[m_dbSo.Considerations.Count - 1].Name;
            m_dbSo.Considerations.Add(new AiConsiderationEntry(StringUtils.MakeUniqueWithNumberSuffix(previousName)));
        }

        // returns the height of a consideration item
        float GetConsiderationItemHeightCallback(int considerationIndex)
        {
            // Hide this item if it doesn't match the search string
            if (StringUtils.DoesNotMatchSearchString(m_searchString, m_dbSo.Considerations[considerationIndex].Name)) { return 0f; }

            // Only display one line if the foldout is close
            if (!m_dbSo.Considerations[considerationIndex].IsFoldoutEnabled) { return EditorGUIUtility.singleLineHeight; }

            // Consideration items are always 5 lines
            return EditorGUIUtility.singleLineHeight * 5;
        }

        // This called back whenever an item is removed from the consideration list
        // Here various things are checked to warn the user if deletion will cause problems.
        void RemoveConsiderationItemCallback(ReorderableList list)
        {
            string considerationName = m_dbSo.Considerations[list.index].Name;
            string msg = "";
            for (int d = 0; d < m_dbSo.Decisions.Count; d++) {
                if (m_dbSo.Decisions[d].ConsiderationNames.Contains(considerationName)) {
                    if (msg.Length > 0) { msg += ", "; }
                    msg += $"{m_dbSo.Decisions[d].Name}";
                }
            }

            bool doDelete = false;
            if (msg.Length > 0) {
                string deleteMessage = $"Warning, deleting {considerationName} will affect these decisions: {msg}";
                if (EditorUtility.DisplayDialog("Are you sure?", deleteMessage, "OK", "Cancel")) { doDelete = true; }
            } else {
                doDelete = true;
            }

            if (doDelete) {
                m_dbSo.Considerations.RemoveAt(list.index);
                EditorUtility.SetDirty(m_dbSo);
            }
        }

        void DoPresetPopup(Rect r, int considerationIndex)
        {
            if (EditorGUI.DropdownButton(r, new GUIContent("Preset"), FocusType.Passive)) {
                // this callback is executed whenever the user selects an item
                Lwduai.ImageComboPopup.SelectItemCallbackDelegate Callback = (idx) =>
                {
                    m_dbSo.Considerations[considerationIndex].Curve = PRESETS[idx].Curve;
                    Repaint();
                };
                ImageComboPopup combo = new ImageComboPopup(m_curveImages, Callback, r.width + 4, 250f, 80f);
                PopupWindow.Show(r, combo);
            }
        }

        // Call this function to display an input-name popup dropdown button.
        // It would be really nice to just do all this with ref instead of switch statements,
        // but C# won't let me pass a ref type to a delegate.
        void DoInputPopup(Rect r, int considerationIndex, string label, InputFieldType type)
        {
            AiConsiderationEntry currentConsideration = m_dbSo.Considerations[considerationIndex];

            EditorGUI.LabelField(r, label);
            r.x += GUI.skin.label.CalcSize(new GUIContent(label)).x;
            string name = "";
            switch (type) {
                case InputFieldType.Input: name = currentConsideration.InputName; break;
                case InputFieldType.Min: name = currentConsideration.Ranges.MinInputName; break;
                case InputFieldType.Max: name = currentConsideration.Ranges.MaxInputName; break;
            }
            if (EditorGUI.DropdownButton(r, new GUIContent(name), FocusType.Passive)) {
                Lwduai.FilterComboPopup.SelectItemCallbackDelegate Callback = (idx, val) => {
                    switch (type) {
                        case InputFieldType.Input: currentConsideration.InputName = val; break;
                        case InputFieldType.Min: currentConsideration.Ranges.MinInputName = val; break;
                        case InputFieldType.Max: currentConsideration.Ranges.MaxInputName = val; break;
                    }
                    Repaint();
                };
                FilterComboPopup combo = new FilterComboPopup(m_inputNames, Callback, "", r.width);
                PopupWindow.Show(r, combo);
            }
        }

        // callback to draw a consideration item
        void DrawConsiderationItemCallback(Rect rect, int considerationIndex, bool isActive, bool isFocused)
        {
            if (m_dbSo == null) { return; }

            AiConsiderationEntry currentConsideration = m_dbSo.Considerations[considerationIndex];

            if (StringUtils.DoesNotMatchSearchString(m_searchString, currentConsideration.Name)) { return; }

            // LINE 1 (always drawn)
            Rect r = new Rect(rect.x, rect.y, 10, EditorGUIUtility.singleLineHeight);

            string foldoutName = currentConsideration.IsFoldoutEnabled ? "" : $"{currentConsideration.Name} (Input: {currentConsideration.InputName})";
            currentConsideration.IsFoldoutEnabled = EditorGUI.Foldout(r, currentConsideration.IsFoldoutEnabled, foldoutName);
            r.x += 20; // skip past the foldout triangle

            // Stop drawing if the foldout is closed
            if (!currentConsideration.IsFoldoutEnabled) { return; }

            // name
            EditorGUIUtility.labelWidth = 50;
            r.width = 250;
            EditorGUI.BeginChangeCheck();
            string oldName = currentConsideration.Name;
            string newName = EditorGUI.TextField(r, new GUIContent("Name:"), currentConsideration.Name, LwduaiWindow.MakeFixedWidthStyle(200, EditorStyles.textField));
            if (EditorGUI.EndChangeCheck()) {
                newName = EnumUtils.MakeStringEnumCompatible(newName);
                if (m_dbSo.FindConsiderationIndex(newName) < 0) {
                    currentConsideration.Name = newName;
                    m_dbSo.RenameConsideration(oldName, newName);
                }
            }
            // input name searchable filter combo popup
            r.x += 260;
            r.width = 200;
            DoInputPopup(r, considerationIndex, "Input:", InputFieldType.Input);

            // LINE 2 (CURVE)
            r.y += 25; // skip to next line
            r.x = rect.x;
            EditorGUIUtility.labelWidth = 18;
            r.width = 80;
            currentConsideration.Curve.Type = (CurveType)EditorGUI.EnumPopup(r, new GUIContent("", "Curve type"), currentConsideration.Curve.Type);
            r.x += r.width + 5;
            currentConsideration.Curve.M = EditorGUI.FloatField(r, new GUIContent("M", "Slope"), currentConsideration.Curve.M);
            r.x += r.width + 5;
            currentConsideration.Curve.K = EditorGUI.FloatField(r, new GUIContent("K", "Scrunch"), currentConsideration.Curve.K);
            r.x += r.width + 5;
            currentConsideration.Curve.B = EditorGUI.FloatField(r, new GUIContent("B", "Y Offset"), currentConsideration.Curve.B);
            r.x += r.width + 5;
            currentConsideration.Curve.C = EditorGUI.FloatField(r, new GUIContent("C", "X Offset"), currentConsideration.Curve.C);
            r.x += r.width + 5;
            currentConsideration.Curve.E = EditorGUI.FloatField(r, new GUIContent("E", "Extra"), currentConsideration.Curve.E);
            r.x += r.width + 5;

            // LINE 3 (RANGES)
            r.y += 25;
            r.width = 60;
            r.x = rect.x;
            EditorGUIUtility.labelWidth = r.width - 15;
            currentConsideration.EnableRanges = EditorGUI.Toggle(r, new GUIContent("Ranges","Default range is 0 to 1. Enable this to set custom ranges. (default false)"), currentConsideration.EnableRanges);
            if (currentConsideration.EnableRanges) {
                // MINIMUM
                r.x += r.width + 5;
                r.width = 45;
                EditorGUIUtility.labelWidth = r.width - 15;
                currentConsideration.Ranges.MinIsVariable = EditorGUI.Toggle(r, new GUIContent("Min: ", "Checking this will allow the min value to be obtained from an input. (default false)"), currentConsideration.Ranges.MinIsVariable);
                r.x += r.width + 5;
                r.width = 170;
                if (currentConsideration.Ranges.MinIsVariable) {
                    DoInputPopup(r, considerationIndex, "", InputFieldType.Min);
                } else {
                    currentConsideration.Ranges.Min = EditorGUI.FloatField(r, new GUIContent("", "The static minimum float value. (default 1)"), currentConsideration.Ranges.Min);
                }
                // MAXIMUM
                r.x += r.width + 5;
                r.width = 55;
                EditorGUIUtility.labelWidth = r.width - 15;
                currentConsideration.Ranges.MaxIsVariable = EditorGUI.Toggle(r, new GUIContent(", Max: ", "Checking this will allow the max value to be obtained from an input. (default false)"), currentConsideration.Ranges.MaxIsVariable);
                r.x += r.width + 5;
                r.width = 170;
                if (currentConsideration.Ranges.MaxIsVariable) {
                    DoInputPopup(r, considerationIndex, "", InputFieldType.Max);
                } else {
                    currentConsideration.Ranges.Max = EditorGUI.FloatField(r, new GUIContent("", "The static maximum float value. (default 1)"), currentConsideration.Ranges.Max);
                }
            }

            // PRESET (FAR RIGHT, TOP)
            r.height = EditorGUIUtility.singleLineHeight;
            r.y = rect.y; // top row
            r.width = 100;
            r.x = rect.x + 525; // far right
            DoPresetPopup(r, considerationIndex);

            // CURVE (FAR RIGHT, BOTTOM)
            r.y = rect.y + EditorGUIUtility.singleLineHeight; // under the preset popup
            r.height = 60;
            r.width = 100;
            currentConsideration.CurveDisplay.keys = new Keyframe[0];
            for (int s = 0; s <= 20; s++) {
                float sample = s * 0.05f;
                currentConsideration.CurveDisplay.AddKey(sample, CurveUtils.Evaluate(sample, currentConsideration.Curve));
            }
            //UnityEditor.AnimationUtility.SetKeyLeftTangentMode(m_dbSo.Considerations[index].CurveDisplay, index, UnityEditor.AnimationUtility.TangentMode.Linear);
            //UnityEditor.AnimationUtility.SetKeyRightTangentMode(m_dbSo.Considerations[index].CurveDisplay, index, UnityEditor.AnimationUtility.TangentMode.Linear);
            EditorGUI.CurveField(r, new GUIContent("", ""), currentConsideration.CurveDisplay, Color.blue, new Rect(0f, 0f, 1f, 1f));
        }
    }
} // namespace

#endif // UNITY_EDITOR