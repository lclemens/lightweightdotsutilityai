// This just has some string utils used by LWDUAI editor windows

namespace Lwduai
{
    public static class StringUtils
    {
        public static string MakeUniqueWithNumberSuffix(string input)
        {
            // Find the position where the number starts
            int lastIndex = input.Length - 1;
            while (lastIndex >= 0 && char.IsDigit(input[lastIndex])) { lastIndex--; }
        
            // Extract the numeric part
            int numStartIndex = lastIndex + 1;
            if (numStartIndex < input.Length) {
                string numberPart = input.Substring(numStartIndex);
                int number;
                if (int.TryParse(numberPart, out number)) {
                    // Increment the number
                    number++;
                    // Replace the old number with the new incremented number in the string
                    return input.Substring(0, numStartIndex) + number.ToString();
                }
            }
            // If no number at the end, return the original string plus 1.
            return input + "1";
        }

        // returns true if the specified string doen't match the search string.
        public static bool DoesNotMatchSearchString(string searchStr, string val)
        {
            return !val.Contains(searchStr, System.StringComparison.OrdinalIgnoreCase);
        }
    }
}