// This is a simple dropdown list drawer that takes a string as a parameter
//
// Example Use:
//   public class MyClass : MonoBehaviour {
//      [ValueDropdown("FetchNames")] // or [ValueDropdown("MyStaticClass.FetchNames")]
//      public List<string> Profiles;
//      public List<string> FetchNames() { return new List<string>() = { "yellow, "red", "green" };
//   }
//
// TODO - this needs an option to only show values that are not already in the list
// and also disable the + button when all the values have been added.

#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Lwduai
{

[CustomPropertyDrawer(typeof(ValueDropdownAttribute))]
public class ValueDropdownDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        ValueDropdownAttribute valueDropdown = attribute as ValueDropdownAttribute;

        if (property.propertyType == SerializedPropertyType.String) {
            // Get the target object
            object target = property.serializedObject.targetObject;

            // Try to find the method in the target instance
            MethodInfo methodInfo = target.GetType().GetMethod(valueDropdown.Callback, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            object classInstance = target;

            // If not found, try to find it as a static method in any class
            if (methodInfo == null) {
                string[] callbackParts = valueDropdown.Callback.Split('.');
                if (callbackParts.Length == 2) {
                    string className = callbackParts[0];
                    string methodName = callbackParts[1];

                    // Get the type of the class
                    Type classType = AppDomain.CurrentDomain.GetAssemblies().SelectMany(assembly => assembly.GetTypes()).FirstOrDefault(t => t.Name == className);
                    if (classType != null) {
                        methodInfo = classType.GetMethod(methodName, BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
                        classInstance = null;
                    }
                }
            }

            if (methodInfo == null) {
                EditorGUI.PropertyField(position, property, label);
                EditorGUI.LabelField(position, "Callback method not found.");
                return;
            }

            // Invoke the method to get the dropdown values
            List<string> dropdownValues = methodInfo.Invoke(classInstance, null) as List<string>;
            if (dropdownValues == null) {
                EditorGUI.PropertyField(position, property, label);
                EditorGUI.LabelField(position, "Callback method did not return a List<string>.");
                return;
            }

            // Create the dropdown menu
            int selectedIndex = Mathf.Max(dropdownValues.IndexOf(property.stringValue), 0);
            selectedIndex = EditorGUI.Popup(position, label.text, selectedIndex, dropdownValues.ToArray());

            // Set the property value
            property.stringValue = dropdownValues[selectedIndex];
        } else {
            EditorGUI.PropertyField(position, property, label);
            EditorGUI.LabelField(position, "Use ValueDropdown with string type.");
        }
    }
}
} // namespace

#endif // UNITY_EDITOR