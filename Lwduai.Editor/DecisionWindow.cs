// This file contains a class for the Decision EditorWindow

#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;
using UnityEditorInternal;
using System.Collections.Generic;
using System.Linq;

namespace Lwduai
{
    public class DecisionWindow : EditorWindow
    {
        bool m_refreshMe = false;
        string m_searchString = "";
        AiDbRecord m_dbSo = null;
        [SerializeField] ReorderableList m_decisionList;
        SerializedObject m_dbProp;
        List<string> m_allConsiderationNames;
        Vector2 m_scrollPos;
        Dictionary<string, ReorderableList> m_considerationListDict = new Dictionary<string, ReorderableList>();

        public void SetDatabase(AiDbRecord db) { m_dbSo = db; }

        void OnGUI()
        {
            // The scriptable object database must be set to display anything
            if (m_dbSo == null) { 
                EditorGUILayout.LabelField("Database is null.");
                m_dbSo = GetWindow<LwduaiWindow>()?.GetDatabase();
                return;
            }

            // update the scriptable object (load it from serialization)
            if (m_dbProp != null) { m_dbProp.Update(); }

            // Top search string field
            if (m_refreshMe) {
                this.Repaint();
                m_refreshMe = false;
            }
            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginChangeCheck();
            m_searchString = GUILayout.TextField(m_searchString, GUI.skin.FindStyle("ToolbarSearchTextField"));
            if (GUILayout.Button("", GUI.skin.FindStyle("ToolbarSearchCancelButton"))) {
                m_searchString = "";
                GUI.FocusControl(null); // Remove focus if cleared
            }
            if (EditorGUI.EndChangeCheck()) { m_refreshMe = true; }
            EditorGUILayout.EndHorizontal();

            // Fetch all the consideration names
            // TODO: Currently this rebuilds every time the gui is drawn, which is probably overkill.
            //       Later this could be only rebuilt whenever consideration names are added/deleted/renamed.
            m_allConsiderationNames = m_dbSo.MakeConsiderationNames();

            // List of decisions with scroll view
            m_scrollPos = GUILayout.BeginScrollView(m_scrollPos, false, false);
            if (m_decisionList == null) {
                m_decisionList = new ReorderableList(m_dbSo.Decisions, typeof(AiDecisionEntry), true, false, true, true);
                m_decisionList.elementHeight = EditorGUIUtility.singleLineHeight * 3f;
                m_decisionList.drawElementCallback = DrawDecisionItemCallback;
                m_decisionList.onAddCallback = AddDecisionItemCallback;
                m_decisionList.elementHeightCallback = GetDecisionItemHeightCallback;
                m_decisionList.onRemoveCallback = RemoveDecisionItemCallback;
                m_dbProp = new SerializedObject(m_dbSo);
                m_decisionList.serializedProperty = m_dbProp.FindProperty("Decisions");
            }
            EditorGUI.BeginChangeCheck();
            m_decisionList.DoLayoutList();
            if (EditorGUI.EndChangeCheck()) { EditorUtility.SetDirty(m_dbSo); }
            GUILayout.EndScrollView();

            // save the scriptable object
            m_dbProp.ApplyModifiedProperties();
        }

        // This is called whenever a decision should be removed
        // If removing a decision will cause upstream errors, the designer is warned.
        void RemoveDecisionItemCallback(ReorderableList list)
        {
            string decisionName = m_dbSo.Decisions[list.index].Name;
            string msg = "";
            for (int p = 0; p < m_dbSo.Profiles.Count; p++) {
                if (m_dbSo.Profiles[p].DecisionNames.Contains(decisionName)) {
                    if (msg.Length > 0) { msg += ", "; }
                    msg += $"{m_dbSo.Profiles[p].Name}";
                }
            }

            bool doDelete = false;
            if (msg.Length > 0) {
                string deleteMessage = $"Warning, deleting {decisionName} will affect these profiles: {msg}";
                if (EditorUtility.DisplayDialog("Are you sure?", deleteMessage, "OK", "Cancel")) { doDelete = true; }
            } else {
                doDelete = true;
            }

            if (doDelete) {
                m_dbSo.Decisions.RemoveAt(list.index); 
                EditorUtility.SetDirty(m_dbSo);
            }
        }

        // callback invoked whenever the height for a particular decision item is needed.
        float GetDecisionItemHeightCallback(int decisionIndex)
        {
            // Hide this item if it doesn't match the search string
            if (StringUtils.DoesNotMatchSearchString(m_searchString, m_dbSo.Decisions[decisionIndex].Name)) { return 0f; }

            // Only display one line if the foldout is close
            if (!m_dbSo.Decisions[decisionIndex].IsFoldoutEnabled) { return EditorGUIUtility.singleLineHeight; }

            // Calculate the height... first line plus all items.
            float height = EditorGUIUtility.singleLineHeight * 5; // initial line plus padding.
            height += Mathf.Max(0, m_dbSo.Decisions[decisionIndex].ConsiderationNames.Count - 1) * EditorGUIUtility.singleLineHeight * 1.3f;
            return height;
        }

        // Callback invoked whenever a new decision is added to the list.
        void AddDecisionItemCallback(ReorderableList list)
        {
            if (m_dbSo.Decisions.Count <= 0) {
                m_dbSo.Decisions.Add(new AiDecisionEntry("NewDecision"));
                return;
            }
            // new name - previous name plus one.
            string previousName = m_dbSo.Decisions[m_dbSo.Decisions.Count - 1].Name;
            m_dbSo.Decisions.Add(new AiDecisionEntry(StringUtils.MakeUniqueWithNumberSuffix(previousName)));
        }

        // called whenever a single decision item needs to be drawn
        void DrawDecisionItemCallback(Rect rect, int decisionIndex, bool isActive, bool isFocused)
        {
            if (m_dbSo == null) { return; }
            if (m_dbSo.Decisions.Count <= 0) { return; }

            AiDecisionEntry currentDecision = m_dbSo.Decisions[decisionIndex];

            if (StringUtils.DoesNotMatchSearchString(m_searchString, currentDecision.Name)) { return; }

            Rect r = new Rect(rect.x, rect.y, 10, EditorGUIUtility.singleLineHeight);

            // LINE 1 (always drawn)
            string foldoutName = currentDecision.IsFoldoutEnabled ? "" : $"{currentDecision.Name} (Weight: {currentDecision.Weight})";
            currentDecision.IsFoldoutEnabled = EditorGUI.Foldout(new Rect(r.x, r.y, 10, EditorGUIUtility.singleLineHeight), currentDecision.IsFoldoutEnabled, foldoutName);
            r.x += 20; // skip past the foldout triangle

            // Stop drawing if the foldout is closed
            if (!currentDecision.IsFoldoutEnabled) { return; }

            // LINE 1
            // enable/disable checkbox
            EditorGUIUtility.labelWidth = 0;
            r.width = 20;
            currentDecision.IsEnabled = EditorGUI.Toggle(r, currentDecision.IsEnabled);
            r.x += 20; // skip past the checkbox

            // name
            EditorGUIUtility.labelWidth = 50;
            r.width = 250;
            EditorGUI.BeginChangeCheck();
            string oldName = currentDecision.Name;
            string newName = EditorGUI.TextField(r, new GUIContent("Name:"), currentDecision.Name, LwduaiWindow.MakeFixedWidthStyle(200, EditorStyles.textField));
            if (EditorGUI.EndChangeCheck()) {
                newName = EnumUtils.MakeStringEnumCompatible(newName);
                if (m_dbSo.FindDecisionIndex(newName) < 0) {
                    currentDecision.Name = newName;
                    m_dbSo.RenameDecision(oldName, newName);
                }
            }

            // weight
            r.x += 260;
            r.width = 150;
            currentDecision.Weight = EditorGUI.FloatField(r, new GUIContent("Weight:"), currentDecision.Weight);

            // LINE 2
            r.y += EditorGUIUtility.singleLineHeight + 5; // skip to next line
            r.x = rect.x;

            // consideration names list
            r.width = 430;
            var decisionsProp = m_dbProp.FindProperty("Decisions");
            var decisionProp = decisionsProp.GetArrayElementAtIndex(decisionIndex);
            SerializedProperty considerationNamesProp = decisionProp.FindPropertyRelative("ConsiderationNames");
            if (considerationNamesProp == null) { UnityEngine.Debug.Log($"considerationNamesProp is null"); return; }
            string key = decisionProp.propertyPath;
            if (key.Length <= 0) { return; }
            if (!m_considerationListDict.TryGetValue(key, out ReorderableList list)) {
                if (considerationNamesProp == null) { UnityEngine.Debug.Log($"considerationNamesProp is null"); }
                if (considerationNamesProp.serializedObject == null) { UnityEngine.Debug.Log($"considerationNamesProp.serializedObject is null"); }
                list = new ReorderableList(considerationNamesProp.serializedObject, considerationNamesProp, true, false, true, true);
                m_considerationListDict.Add(key, list);
                list.onCanAddCallback = (list) => CanAddConsiderationCallback(decisionIndex);
                list.drawElementCallback = (rect, index, isActive, isFocused) => { DrawConsiderationNameItemCallback(rect, index, isActive, isFocused, decisionIndex); };
                list.onAddCallback = (list) => AddConsiderationCallback(decisionIndex);
            }
            list.DoList(r);
        }

        // Called whenever a consideration name is added to any consideration names list.
        // This is intercepted so that we can ensure that the newly added
        // consideration name won't be a duplicate of a previous one.
        void AddConsiderationCallback(int decisionIndex)
        {
            // attempt to add a decision name that doesn't already exist
            for (int i = 0; i < m_allConsiderationNames.Count; i++) {
                if (!m_dbSo.Decisions[decisionIndex].ConsiderationNames.Contains(m_allConsiderationNames[i])) {
                    m_dbSo.Decisions[decisionIndex].ConsiderationNames.Add(m_allConsiderationNames[i]);
                    return;
                }
            }
        }

        // This callback is invoked whenever a consideration name item needs to be drawn.
        void DrawConsiderationNameItemCallback(Rect rect, int considerationNameIndex, bool isActive, bool isFocused, int decisionIndex)
        {
            string selectedOption = m_dbSo.Decisions[decisionIndex].ConsiderationNames[considerationNameIndex];

            // make a list of all decisions that are not already in the list
            List<string> filteredList = m_allConsiderationNames.Except(m_dbSo.Decisions[decisionIndex].ConsiderationNames).ToList();
            filteredList.Add(selectedOption);

            // setup position rect
            EditorGUIUtility.labelWidth = 0;
            Rect r = new Rect(rect.x, rect.y + 5, rect.width, EditorGUIUtility.singleLineHeight);

            // make a searchable filter combo popup
            if (EditorGUI.DropdownButton(r, new GUIContent(selectedOption), FocusType.Passive)) {
                PopupWindow.Show(r, new FilterComboPopup(filteredList, ItemSelectedCallback, "", r.width));
            }
            void ItemSelectedCallback(int index, string value)
            {
                m_dbSo.Decisions[decisionIndex].ConsiderationNames[considerationNameIndex] = value;
                Repaint();
            }
        }

        // callback to determine if the add button for a consideration list is enabled.
        bool CanAddConsiderationCallback(int decisionIndex)
        {
            // if the current decision names has all the same items as the list of all available decision names,
            // then return false, which will disable the addition button.
            return m_allConsiderationNames.Except(m_dbSo.Decisions[decisionIndex].ConsiderationNames).Count() > 0;
        }
    }
} // namespace

#endif // UNITY_EDITOR