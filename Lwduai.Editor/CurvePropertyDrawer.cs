// This contains property drawer stuff to allow adding animation curves to a scriptable object or standard editor script.
// credit: glamberous in the TMG discord and Obay Naeem
// https://www.linkedin.com/pulse/bounded-grid-animation-curves-unity-obay-naeem/

// ADD TO PROJECT
using UnityEngine;
using UnityEditor;

namespace Lwduai
{
    // EDIT FOLDER
    [CustomPropertyDrawer(typeof(BoundedCurveAttribute))]
    public class BoundedCurveDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            BoundedCurveAttribute boundedCurve = (BoundedCurveAttribute)attribute;
            return EditorGUIUtility.singleLineHeight * boundedCurve.guiHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            BoundedCurveAttribute boundedCurve = (BoundedCurveAttribute)attribute;
            EditorGUI.BeginProperty(position, label, property);
            property.animationCurveValue = EditorGUI.CurveField(position, label, property.animationCurveValue, Color.green, boundedCurve.bounds);
            EditorGUI.EndProperty();
        }
    }
} // namespace