// This system runs a job that will loop through all health stations for every agent.
// It does two things:
//    1) It keeps every agent's HealthInfluenceData up to date.
//    2) It increments health for agents within a health station's influence radius.
//
// The reason I'm not using overlapaabb spatial partitioning is because I need to know the closest health station
// for every enemy - which requires a per-entity loop. If I'm going to do a per-entity loop, I might as well
// just set each agent's health influences at the same time.
//
// This is an example of an expensive input system that runs at an interval no matter what.
// It always runs because it independently will start charging health for agents whenever they're near a health station.
// So for Utility AI, just add a line to fill the MyHealAmount slot of the input buffer in ExampleCheapInputSystem.
//
// The system runs on a 0.25ms interval and is scheduled per-entity in parallel.


using Unity.Entities;
using Unity.Collections;
using Unity.Burst;
using Unity.Transforms;
using Unity.Mathematics;
using Lwduai;

namespace LwduaiExample
{

[BurstCompile]
public partial struct HealthStationInfluenceJob : IJobEntity
{
    //[ReadOnly][DeallocateOnJobCompletion] public NativeArray<LocalToWorld> StationLtws;
    //[ReadOnly][DeallocateOnJobCompletion] public NativeArray<Entity> StationEntities;
    //[ReadOnly][DeallocateOnJobCompletion] public NativeArray<HealthRechargeData> StationRecharges;

    [ReadOnly][DeallocateOnJobCompletion] public NativeArray<Entity> StationEntities;
    [ReadOnly] public ComponentLookup<HealthRechargeData> StationRechargeLookup;
    [ReadOnly] public ComponentLookup<LocalToWorld> StationLtwLookup;

    // For all agents, for all health stations...
    // This is being done backwards (for all agents) because there are likely way more agents than health stations
    // and I want to access the write parameters for the agents without lookups.
    // Note that here I can access the health data directly and increment it instead of using a health-modifier
    // component because each agent is being accessed directly and there will be no race condition.
    //
    // Note that this is just a normal system unrelated to AI but with one minor exception:
    // It takes the inputs as a parameter, and early-outs anytime the heal-amount input is not marked as needing calculation.
    // If not early-outing, then it will copy the heal-amount (if any) into the input buffer.
    [BurstCompile]
    public void Execute(ref HealthData health, ref HealthInfluenceData healthInfluence, in MaxHealthData maxHealth, in LocalTransform lt)
    {
        // reset health influence for this agent (it will be refreshed below)
        healthInfluence.Amount = 0f;
        healthInfluence.NearestHealthStationEntity = Entity.Null;
        healthInfluence.DistanceToNearestStation = float.MaxValue;

        float nearestDistSq = float.MaxValue;

        // For each health station...
        for (int i = 0; i < StationEntities.Length; i++)
        {
            //HealthRechargeData charge = StationRecharges[i];
            HealthRechargeData charge = StationRechargeLookup[StationEntities[i]];
            //float distSq = math.distancesq(lt.Position, StationLtws[i].Position);
            float distSq = math.distancesq(lt.Position, StationLtwLookup[StationEntities[i]].Position);

            // if this health station is able to influence the agent...
            if (distSq <= (charge.Radius * charge.Radius))
            {
                // add the influence amount for this agent
                healthInfluence.Amount += charge.Amount;

                // add to this agent's health (clamp to max-health value)
                health.Health += charge.Amount;
                if (health.Health > maxHealth.Health) { health.Health = maxHealth.Health; }
            }

            if (distSq < nearestDistSq)
            {
                healthInfluence.NearestHealthStationEntity = StationEntities[i];
                nearestDistSq = distSq;
            }
        }

        // if at least one health station was found, set the distance-to-nearest value
        // if not, the value will be float.MaxValue
        if (healthInfluence.NearestHealthStationEntity != Entity.Null)
        {
            healthInfluence.DistanceToNearestStation = math.sqrt(nearestDistSq);
        }
    }
}

[BurstCompile]
[RequireMatchingQueriesForUpdate]
public partial struct HealthStationInfluenceSystem : ISystem
{
    UpdateTimer m_timer;
    EntityQuery m_healthStationQuery;

    void OnCreate(ref SystemState state)
    {
        m_timer = new UpdateTimer(0.25f); // this MUST be 0.25 (the way it's coded right now)
        m_healthStationQuery = state.GetEntityQuery(ComponentType.ReadOnly<LocalToWorld>(), ComponentType.ReadOnly<HealthRechargeData>());
    }

    [BurstCompile]
    void OnUpdate(ref SystemState state)
    {
        if (m_timer.IsNotReady(SystemAPI.Time.DeltaTime)) { return; }

        HealthStationInfluenceJob job = new HealthStationInfluenceJob();

        // sync way
        job.StationEntities = m_healthStationQuery.ToEntityArray(Allocator.TempJob);
        // async way
        //job.StationLtws = m_healthStationQuery.ToComponentDataArray<LocalToWorld>(Allocator.TempJob);
        //job.StationRecharges = m_healthStationQuery.ToComponentDataArray<HealthRechargeData>(Allocator.TempJob);
        job.StationLtwLookup = SystemAPI.GetComponentLookup<LocalToWorld>(true);
        job.StationRechargeLookup = SystemAPI.GetComponentLookup<HealthRechargeData>(true);
        state.Dependency = job.ScheduleParallel(state.Dependency);
    }
}

} // namespace