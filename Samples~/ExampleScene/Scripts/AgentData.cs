// This file holds various structs for an example agent

using Unity.Entities;

namespace LwduaiExample
{

public struct MoveAgentTag : IComponentData { }

// the target chooser sets this
public struct AttackTargetData : IComponentData
{
    public Entity TargetEntity;
    public float Distance;
}

// this is the final target used by the movement system.
public struct TargetData : IComponentData
{
    public Entity TargetEntity;
}

public struct MoveSpeedData : IComponentData
{
    public float Speed;
}

public struct HealthData : IComponentData
{
    public float Health;
}

public struct MaxHealthData : IComponentData
{
    public float Health;
}

public struct AttackCooldownData : IComponentData
{
    public float Time;
    public float Interval;

    public AttackCooldownData(float i)
    {
        Time = 0f;
        Interval = i;
    }
    public void Reset() { Time = Interval; }
    public bool IsCool() { return Time <= 0; }
    public void Update(float delta) { Time -= delta;  }
    public bool IsCoolWithUpdate(float delta)
    {
        Time -= delta;
        return Time <= 0;
    }
}

public struct AttackData : IComponentData
{
    public float Range;
    public float Damage;
}

public struct EnemyTag : IComponentData { }
public struct PlayerTag : IComponentData { }

public enum FactionType : byte { Enemy, Player }

public struct FactionData : IComponentData
{
    public FactionType Faction;
}

public struct AgentTag : IComponentData { }

// This gets placed on an agent, and it represents the amount of health that an agent is receiving.
// Multiple health stations can influence an agent at the same time if their radii overlap.
public struct HealthInfluenceData : IComponentData
{
    public float Amount; // the total amount of influence from all health stations
    public Entity NearestHealthStationEntity; // holds the nearest health station
    public float DistanceToNearestStation; // distance from agent to the nearest health station
}

} // namespace