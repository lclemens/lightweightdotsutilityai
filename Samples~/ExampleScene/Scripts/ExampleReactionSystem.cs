// This is an example of a per-agent reaction system that reacts depending what's currently in each AiActionData.
// Note that you can divide this up into as many systems and jobs as you want.
// In this example, one system is used with a few jobs.
//
// A "reaction" system is really nothing special... its just normal game logic that uses the values in AiActionData.

using Unity.Entities;
using Unity.Burst;
using Unity.Rendering;
using Unity.Mathematics;
using Lwduai;

namespace LwduaiExample
{

// This job will set the current target based on whether the action is move to health station or move to attack target.
// The AgentMovementSystem will use this target to move the agent if it is not null.
// Technically this could have been handled in the agent movement system, but I wanted an example where
// the agent movement is an independent system that isn't aware of AI.
[BurstCompile]
public partial struct ExampleMovementReactionJob : IJobEntity
{
    [BurstCompile]
    public void Execute(ref TargetData target, in AiActionData action, in AttackTargetData attackTarget, in HealthInfluenceData healthTarget)
    {
        // when target is not null the movement system will take over
        if ((AiDecisionType)action.Action == AiDecisionType.MoveToTarget) {
            target.TargetEntity = attackTarget.TargetEntity;
        } else if ((AiDecisionType)action.Action == AiDecisionType.MoveToHealthStation) {
            target.TargetEntity = healthTarget.NearestHealthStationEntity;
        } else {
            target.TargetEntity = Entity.Null; // stops movement
        }
    }
}

// This job will attack and damage the target entity if the current action is set to attack
// note that because this modifies the target health directly, if two agents attack the same
// enemy in the same frame, only one of the attacks will register.
[BurstCompile]
public partial struct ExampleAttackReactionJob : IJobEntity
{
    public ComponentLookup<HealthData> HealthLookup;
    public EntityCommandBuffer EcbBegin;
    public float DeltaTime;
    public short AttackId; // optional alternative to using the AiDecisionType.Attack enum

    [BurstCompile]
    public void Execute(ref AttackCooldownData cooldown, in AiActionData action, in AttackTargetData target, in AttackData attack)
    {
        // note that we could instead use:
        //   if ((AiDecisionType)action.Action != AiDecisionType.Attack) { return; }
        // however, to demonstrate that using enums is optional,
        // AttackId is fetched from the global singleton database.
        if (action.Action != AttackId) { return; }

        if (target.TargetEntity == Entity.Null) { return; }
        if (!HealthLookup.HasComponent(target.TargetEntity)) { return; }

        float currentHealth = HealthLookup[target.TargetEntity].Health;
        float newHealth = currentHealth - attack.Damage;
        if (newHealth > 0) {
            HealthLookup[target.TargetEntity] = new HealthData { Health = currentHealth - attack.Damage };
        } else {
            // this agent kicked the bucket, so destroy it.
            HealthLookup[target.TargetEntity] = new HealthData { Health = 0f };
            EcbBegin.DestroyEntity(target.TargetEntity);
        }
        cooldown.Reset();
    }
}

// This job will set the agent's color based on its current action.
[BurstCompile]
public partial struct ExampleColorReactionJob : IJobEntity
{
    [BurstCompile]
    public void Execute(in AiActionData action, ref URPMaterialPropertyBaseColor baseColor)
    {
        UnityEngine.Color color = UnityEngine.Color.white;
        switch ((AiDecisionType)action.Action) {
            case AiDecisionType.Attack: color = UnityEngine.Color.black; break;
            case AiDecisionType.MoveToHealthStation: color = UnityEngine.Color.blue; break;
            case AiDecisionType.IdleHeal: color = UnityEngine.Color.red; break;
            case AiDecisionType.MoveToTarget: color = UnityEngine.Color.yellow; break;
            case AiDecisionType.IdleNoTarget: color = UnityEngine.Color.grey; break;
            case AiDecisionType.IdleBetweenAttacks: color = UnityEngine.Color.grey; break;
        }
        baseColor.Value = new float4(color.r, color.g, color.b, color.a);
    }
}

// This system runs several jobs which react based on the current action.
// Note that you could break this into multiple systems if you want,
// or you could do it all in a single system and single job.
// How you organize your "reaction" systems is up to you.
[BurstCompile]
[RequireMatchingQueriesForUpdate]
public partial struct ExampleReactionSystem : ISystem
{
    short m_AttackId; // Example of avoiding enums by caching an ID to avoid looking it up every time.

    [BurstCompile]
    void OnCreate(ref SystemState state)
    {
        m_AttackId = -1; // init the cached ID to -1.
        // This ensures that the reaction jobs only launch whenever there are fresh new AI scores.
        // If you don't want to use this, you can still read the cached AiActionData anytime by any system.
        // However, it's useful to limit this to the last frame for most reaction systems.
        state.RequireForUpdate<AiLastFrameTag>();
    }

    [BurstCompile]
    void OnUpdate(ref SystemState state)
    {
        // fetch and cache the attack ID if it hasn't been fetched already. (Alternative to using Enum)
        if (m_AttackId == -1) { m_AttackId = SystemAPI.GetSingleton<AiTableData>().FindDecisionId("Attack"); }

        // movement job
        ExampleMovementReactionJob movementJob = new ExampleMovementReactionJob();
        state.Dependency = movementJob.ScheduleParallel(state.Dependency);

        // attack job
        ExampleAttackReactionJob attackJob = new ExampleAttackReactionJob();
        attackJob.DeltaTime = SystemAPI.Time.DeltaTime;
        attackJob.EcbBegin = SystemAPI.GetSingleton<BeginSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(state.WorldUnmanaged);
        attackJob.HealthLookup = SystemAPI.GetComponentLookup<HealthData>(false);
        attackJob.AttackId = m_AttackId; // pass cached id to job (Alternative to using Enum)
        state.Dependency = attackJob.Schedule(state.Dependency);

        // color job
        ExampleColorReactionJob colorJob = new ExampleColorReactionJob();
        state.Dependency = colorJob.ScheduleParallel(state.Dependency);
    }
}

} // namespace