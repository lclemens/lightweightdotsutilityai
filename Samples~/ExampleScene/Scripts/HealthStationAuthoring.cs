// This is an authoring/baker for a health station. 

using UnityEngine;
using Unity.Entities;

namespace LwduaiExample
{

public struct HealthRechargeData : IComponentData
{
    public float Radius;
    public float Amount; // charge amount per interval (4x per second)
}

public class HealthStationAuthoring : MonoBehaviour
{
    [Tooltip("The amount to charge ever 1/4 second")]
    public float DefaultAmount = 0.25f;

    [Tooltip("The influence radius of the health recharging station.")]
    public float DefaultRadius = 10f;
}

public class HealthStationBaker : Baker<HealthStationAuthoring>
{
    public override void Bake(HealthStationAuthoring authoring)
    {
        Entity entity = GetEntity(authoring, TransformUsageFlags.None);
        AddComponent(entity, new HealthRechargeData { Radius = authoring.DefaultRadius, Amount = authoring.DefaultAmount });
    }
}

} // namespace