<h1>Lightweight DOTS Utility AI Changelog</h1>

Version 1.0.24
<ul>
	<li>Undid the memory allocation stuff in AiTextGizmo. That was a stupid idea.</li>
</ul>
Version 1.0.23
<ul>
	<li>Improved the performance of AiTextGizmo by reducing allocations.</li>
	<li>Changed the "Health" field of AiTextGizmo to work for any custom input in quick-info mode.</li>
</ul>
Version 1.0.22
<ul>
	<li>Made a small fix to the LWDUI Window so everything still works if the developer doesn't have a backslash at the end of the enum path.</li>
</ul>
Version 1.0.21
<ul>
	<li>Moved ValueDropDown.cs into the Editor folder (merge request by Laicasaane).</li>
</ul>
Version 1.0.20
<ul>
	<li>Replaced the profile list of shorts on the AiAgentAuthoring script with a dropdown list of profile strings.</li>
</ul>
Version 1.0.19
<ul>
	<li>Made a few slight tweaks to the preset dropdown to make it look nicer.</li>
</ul>
Version 1.0.18
<ul>
	<li>Changed the path used for loading package images.</li>
</ul>
Version 1.0.17
<ul>
	<li>Changed MKBC to MKBCE.</li>
	<li>Added a popup for choosing a preset.</li>
	<li>Fixed a bug in the Square curve type evaluation.</li>
</ul>
Version 1.0.16
<ul>
	<li>Fixed the camera controller in the example scene to support the new input system if it's in use.</li>
</ul>
Version 1.0.15
<ul>
	<li>Added cancel buttons to the search fields at the top of each window.</li>
	<li>Added foldouts for list items.</li>
	<li>Modified the new item function for every window so that new items always have unique names.</li>
	<li>Added verificaton of names so they are always enum-compatible.</li>
</ul>
Version 1.0.14
<ul>
	<li>Added search fields at the top of each window.</li>
	<li>Replaces all combo boxes with combo boxes that can do filtered searching.</li>
</ul>
Version 1.0.13
<ul>
	<li>Fixed a bug that caused restarting unity to lose edits in the GUI.</li>
	<li>Added logic to save/load the last-used enum output folder.</li>
	<li>Fixed a bug where the window pointers didn't get saved after restarting unity.</li>
</ul>
Version 1.0.12
<ul>
	<li>Fixed a bug that caused an error building in release mode.</li>
</ul>
Version 1.0.11
<ul>
	<li>Put a namespace around the example scene scripts.</li>
</ul>
Version 1.0.10
<ul>
	<li>Renamed Images to Images~</li>
</ul>
Version 1.0.9
<ul>
	<li>Added an Off option to AiTextGizmo.</li>
	<li>Added the ability to fetch Ids via strings instead of using Enums.</li>
</ul>

Version 1.0.8
<ul>
	<li>The reliance on Odin Inspector has been completely removed.</li>
	<li>AiManager is no longer needed.</li>
	<li>The project is now a package that can be imported.</li>
	<li>The core no longer uses enum types.</li>
</ul>

Version 1.0.0
<ul>
	<li>New GUI with simplified editing and checks to ensure non-duplicate names.</li>
	<li>Added GUI checks to alert the user when deleting a name will affect downstream connections.</li>
	<li>Added logic to automatically rename downstream components when renaming an upstream list item.</li>
	<li>Combined the 4 scriptable objects into a single database.</li>
</ul>